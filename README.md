# Airport - automatic landing system

Airport - automatic landing system: The application simulates the operation of an airport with an automatic landing system. The project includes the use of technologies such as client / server communication using sockets, connection pool for the database, multithreading.


## Technical assumptions:

- Client / server working on sockets.
- Storing information about established connections, collisions and successful landings in a database.
- Clients are aircraft.
- Server is an automatic landing system.


## Business assumptions

- The airport has 2 runways.
- There can be a maximum of 100 aircraft in the airspace of the airport, any further aircraft should be told to find another airport when trying to connect and disappear from airspace.
- The airport commands a space of 10 km by 10 km and an altitude of 5 km.
- Each arriving aircraft has fuel for a 3 hour flight.
- Air corridors for landing must be established.
- Aircraft appear at a random point on the boundary of the airspace, OUTSIDE the air corridors at an altitude of 2km to 5km.
- Bringing the aircraft down to the ground involves directing it into an air corridor and keeping it clear to the runway, on the runway the aircraft must be 0m high otherwise the landing is considered a failure and a collision occurs.
- If two aeroplanes come within 10m of each other, a collision is deemed to have occurred.
- If an aircraft runs out of fuel while waiting, we consider that a collision has occurred.


## Project Structure

### client.py
Description: This module represents the client in the system, which is an airplane.
Main Classes and Functions:
- Client: Class representing the client. Contains methods for communication with the server, sending and receiving data, and handling various airplane states.
	- run(): Main method that runs the client's loop, establishing connection with the server and handling data transmission.
	- connect(): Method for establishing a connection with the server.
	- take_data(), send_data(data): Methods for receiving and sending data to/from the server.
	- process_message(message): Method for processing received messages.
	- send_current_position(): Method for sending the current position of the airplane to the server.
	- clean_up(): Method for closing the connection and cleaning up resources.
	
### server.py
Description: This module represents the server in the system, which is the automatic landing system.
Main Classes and Functions:
- Server: Class representing the server. Contains methods for managing connections with clients, processing data, and handling air traffic.
	- connect(): Method for establishing a connection and listening for new connections.
	- new_connection(): Method for handling new connections from clients.
	- start(): Main method that runs the server.
- ClientHandler: Class representing a thread that handles a single client.
	- run(): Main method that runs the client's handling thread loop.
	- wait_for_confirmation(), handle_confirmation(confirmation_type): Methods for handling various types of messages from the client.
	- send_identification_data(): Method for sending identification data to the client.
	- send_landing_strip_data(): Method for sending landing strip data to the client.
	- update_and_check_current_position_data(position): Method for updating and checking the client's position.
	
### plane.py
Description: This module represents an airplane as a thread that performs operations related to flying and landing.
Main Classes and Functions:
- Plane: Class representing the airplane as a thread.
	- run(): Main method that runs the airplane thread.
	- set_starting_place(): Method for setting the initial position of the airplane.
	- fly_to_the_assigned_coordinates(): Method for flying to the assigned coordinates.
	- circle_flight(): Method for circling flight while waiting for landing.
	- landing_approach(): Method for landing approach.
	- fuel_consumption(): Method for fuel consumption.
	- initialisation_stage(), landing_procedure(): Methods for initializing and handling the landing procedure.

### airport.py
Description: This module manages the airport, its resources, and operations related to air traffic control.
Main Classes and Functions:
- Airport: Class representing the airport.
	- create_waiting_corridors(): Method for creating waiting corridors.
	- set_waiting_corridor_coordinates(object_id): Method for setting waiting corridor coordinates for an airplane.
	- check_landing_corridors(), set_landing_corridor(marker, identification), release_landing_corridor(identification): Methods for managing landing corridors.
	- check_distance(plane_id, plane_position): Method for checking the distance between airplanes to detect collisions.

### data_tools.py
Description: This module contains tools for data processing.
Main Classes and Functions:
- JsonHandler: Class for encoding and decoding JSON data.
	- decode(json_data), encode(data): Methods for decoding and encoding JSON data.

### logger.py
Description: This module contains the logger configuration for logging events in the system.
Main Classes and Functions:
- Logger: Class configuring the logger.
	- get_logger(): Method that returns the configured logger.

### visualisation.py
Description: This module handles the visualization of air traffic at the airport.
Main Classes and Functions:
- AirportVisualiser: Class for visualizing air traffic.
	- run(): Method that runs the visualization.

### technical_assumptions.py
Description: This module contains technical assumptions used in the application.
- Main Variables:
	- refreshing_condition, airplane_speed, airplane_fuel, fuel_consumption: Variables defining technical parameters of airplanes.

### data_base/database.py
Description: This module contains functions for managing the database.
Main Classes and Functions:
- DatabaseConfiguration: Class configuring the database connection.
- DatabaseHandler: Class managing the database connection.
	- open_connection(), close_connection(), ensure_database_exists(), create_airport_table(), clear_table(): Methods for managing the connection and tables in the database.
- PlaneHandler: Class managing airplane data in the database.
	- insert_registration_data(id), update_specific_status(object_id, status, value): Methods for managing airplane data.

### data_base/connection_pool.py
Description: This module handles the database connection pool.
Main Classes and Functions:
- ConnectionPool: Class managing the connection pool.
	- get_connection(), release_connection(connection), clean_up_pool(): Methods for managing the connection pool.
	
	
## Run application

Follow the steps below to ensure that the application runs correctly:

### Python version:
Make sure you have Python version 3.11 or later installed on your system. If not, you can download it from the official Python website.
You can also check your current Python version by running python --version in a terminal.

### Repository configuration:
Clone the repository to your local machine using Git. If you have not already done so, execute:
```
git clone https://gitlab.com/kristoveq/airport-automatic-landing-system/-/tree/multithreaded_client?ref_type=heads
```

### Dependency installation:
Navigate to the folder of the cloned repository.
Install the necessary Python modules by executing:
```
pip install -r requirements.txt
```

### PostgreSQL installation:
Make sure you have PostgreSQL version 16.1 installed; if not, download and install it from the official PostgreSQL website.
Launching the application:


### Log file and database:
When you run your application for the first time, a log file will be created in the root folder. You can check this log file for errors or debugging information.
In addition, a PostgreSQL database named 'airport' will be created, along with a table named 'airport'.


## Example use

Open a terminal or command line.

### Running the Server
In one terminal, run the server.py script:
```
python server.py
```
This will initialize the server, which will listen for incoming connections from clients.

### Running the Client
In another terminal, run the client.py script:
```
python client.py
```
This will initialize a client that will connect to the server and begin the communication process.


## Visualisation

To visualize the air traffic, run the server and the visualization module will be automatically started. The visualization will show the positions of the planes in the airspace.


## Example Code Snippets

### Starting the Server
```
# server.py
if __name__ == '__main__':
    server = Server()
    server.connect()
    threading.Thread(target=server.start).start()
    visualisation = AirportVisualiser(server.airport, server.airport.air_traffic, server.pos_lock)
    visualisation.run()
```

### Starting the Client
```
# client.py
if __name__ == '__main__':
    while True:
        interval = random.uniform(0.5, 3)
        time.sleep(interval)
        client = Client()
        threading.Thread(target=client.run).start()
```

These examples demonstrate how to initialize and run the server and client components of the application. For more detailed usage and configuration options, please refer to the source code and comments within each file.


