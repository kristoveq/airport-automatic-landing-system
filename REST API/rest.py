from flask import Flask, jsonify
from app import Launcher
from threading import Thread


class AirportSystemAPI:
    """
    The `AirportSystemAPI` class sets up a Flask-based API for managing the automated
    airport system, including the ability to start, pause, resume, and shut down
    the system, as well as retrieve information about system uptime, active planes,
    collisions, and landings.
    """
    def __init__(self):
        """
        Initializes the `AirportSystemAPI` instance.

        Attributes:
            self.app (Flask): The Flask application instance used to manage HTTP requests.
            self.launcher (Launcher): An instance of the `Launcher` class responsible for
                                 controlling the airport system's operations.
        Calls:
            setup_routes(): Configures the API routes and maps them to their handler methods.
        """
        self.app = Flask(__name__)
        self.launcher = Launcher()
        self.setup_routes()

    def setup_routes(self):
        """
        Sets up the API routes and defines their respective endpoints.

        Endpoints:
           - POST /start-system: Initiates the start of the airport system.
           - POST /pause-system: Pauses the current operation of the airport system.
           - POST /resume-system: Resumes the paused airport system.
           - POST /shutdown-system: Initiates the shutdown process for the airport system.
           - GET /uptime-system: Retrieves the uptime of the system in seconds.
           - GET /active-planes: Returns the count of active planes in the airspace.
           - GET /collisions-info: Provides information about detected collisions.
           - GET /landings-info: Lists the IDs of planes that have successfully landed.
           - GET /active-info: Lists the IDs of currently active planes.
           - GET /plane-info/<int:plane_id>: Retrieves information about a specific plane.
        """
        @self.app.route('/start-system', methods=['POST'])
        def start_system():
            return self.start_system()

        @self.app.route('/pause-system', methods=['POST'])
        def pause_system():
            return self.pause_system()

        @self.app.route('/resume-system', methods=['POST'])
        def resume_system():
            return self.resume_system()

        @self.app.route('/shutdown-system', methods=['POST'])
        def shutdown_system():
            return self.shutdown_system()

        @self.app.route('/uptime-system', methods=['GET'])
        def uptime_system():
            return self.get_uptime()

        @self.app.route('/active-planes', methods=['GET'])
        def active_planes_amount():
            return self.get_active_planes_amount()

        @self.app.route('/collisions-info', methods=['GET'])
        def collisions_info():
            return self.get_collisions_info()

        @self.app.route('/landings-info', methods=['GET'])
        def landings_info():
            return self.get_landings_info()

        @self.app.route('/active-info', methods=['GET'])
        def active_planes_info():
            return self.get_active_planes_info()

        @self.app.route('/plane-info/<int:plane_id>', methods=['GET'])
        def plane_info(plane_id):
            return self.get_specific_plane_info(plane_id)

    def start_system(self):
        """
        Start the airport system.

        This method attempts to start the background tasks of the airport system using the
        `Launcher` class. If successful, it returns a success message. If an error occurs,
        it returns an error message and a 500 status code.

        Returns:
            Response: A Flask JSON response with a success message and status 200, or an error
                      message and status 500 if an exception is raised.
        """
        try:
            self.launcher.start_background_tasks()
            return jsonify({"message": "Airport system started successfully."}), 200
        except Exception as e:
            return jsonify({"Error during starting airport system": str(e)}), 500

    def pause_system(self):
        """
        Pause the airport system.

        This method attempts to pause the airport system using the `Launcher` class. If
        successful, it returns a success message. If an error occurs, it returns an error
        message and a 500 status code.

        Returns:
            Response: A Flask JSON response with a success message and status 200, or an error
                     message and status 500 if an exception is raised.
        """
        try:
            self.launcher.pause_system()
            return jsonify({'message': 'Airport system paused.'}), 200
        except Exception as e:
            return jsonify({'Error during pausing system.': str(e)}), 500

    def resume_system(self):
        """
        Resume the airport system.

        This method attempts to resume the paused airport system using the `Launcher` class.
        If successful, it returns a success message. If an error occurs, it returns an error
        message and a 500 status code.

        Returns:
            Response: A Flask JSON response with a success message and status 200, or an error
                      message and status 500 if an exception is raised.
        """
        try:
            self.launcher.resume_system()
            return jsonify({'message': 'Airport system resumed'}), 200
        except Exception as e:
            return jsonify({'Error during resuming system': str(e)}), 500

    def shutdown_system(self):
        """
        Initiate the shutdown of the airport system.

        This method attempts to shut down the airport system gracefully using `Launcher` class.
        If successful, it returns a success message. If an error occurs, it returns an error
        message and a 500 status code.

        Returns:
            Response: A Flask JSON response with a success message and status 200, or an error
                      message and status 500 if an exception is raised.
        """
        try:
            self.launcher.shutdown_system()
            return jsonify({'message': 'Airport system shutdown initiaded.'}), 200
        except Exception as e:
            return jsonify({'Error during shutting down the system': str({e})}), 500

    def get_uptime(self):
        """
        Retrieve the uptime of the airport system.

        This method fetches uptime of server in seconds using `Launcher` class.
        If successful, it returns uptime in a formatted message. If an error occurs, it
        returns an error message and a 500 status code.

        Returns:
            Response: A Flask JSON response with uptime message and status 200, or an error
                      message and status 500 if an exception is raised.
        """
        try:
            result = self.launcher.server.uptime()
            return jsonify({'message': f'Server uptime: {result} seconds.'}), 200
        except Exception as e:
            return jsonify({'Error during get server uptime': str({e})}), 500

    def get_active_planes_amount(self):
        """
        Get the number of active planes in airspace.

        This method calculates number of active planes currently being tracked in the
        airspace using `Launcher` class. If successful, it returns the count in a
        formatted message. If an error occurs, it returns an error message and a 500 status code.

        Returns:
            Response: A Flask JSON response with count of active planes and status 200,
                      or an error message and status 500 if an exception is raised.
        """
        try:
            amount = len(self.launcher.server.airport.air_traffic)
            return jsonify({'message': f'Amount of active planes: {amount}'}), 200
        except Exception as e:
            return jsonify({'Error during get amount of active planes': str({e})}), 500

    def get_collisions_info(self):
        """
        Retrieve information about detected collisions.

        This method fetches information about any detected collisions from database
        using `Launcher` class. If no collisions have been detected, it returns a message
        indicating so. If collisions are found, it returns a list of the IDs of collided planes.
        If an error occurs, it returns an error message and a 500 status code.

        Returns:
            Response: A Flask JSON response with the collision information and status 200,
                      or an error message and status 500 if an exception is raised.
        """
        try:
            values = self.launcher.db.get_status_info('collision')
            if not values:
                return jsonify({'message': 'System has not detected any collisions so far.'}), 200

            string_values = [str(value[0]) for value in values]
            result = ', '.join(string_values)
            return jsonify({'message': f'ID of collided planes: {result}.'}), 200
        except Exception as e:
            return jsonify({'Error during get info about collisions': str({e})}), 500

    def get_landings_info(self):
        """
        Retrieve information about successful landings.

        This method fetches information about planes that have successfully landed using
        `Launcher` class. If no planes have landed, it returns a message indicating so.
        If landings are found, it returns a list of the IDs of landed planes. If an error
        occurs, it returns an error message and a 500 status code.

        Returns:
           Response: A Flask JSON response with the landing information and status 200,
                     or an error message and status 500 if an exception is raised.
        """
        try:
            values = self.launcher.db.get_status_info('landed')
            if not values:
                return jsonify({'message': 'System has not detected any landings so far.'}), 200

            string_values = [str(value[0]) for value in values]
            result = ', '.join(string_values)
            return jsonify({'message': f'ID of landed planes: {result}.'}), 200
        except Exception as e:
            return jsonify({'Error during get info about landings': str({e})}), 500

    def get_active_planes_info(self):
        """
        Retrieve information about active planes in the airspace.

        This method fetches information about currently active planes using `Launcher` class.
        If no active planes are detected, it returns a message indicating so. If active planes
        are found, it returns a list of their IDs. If an error occurs, it returns an error
        message and a 500 status code.

        Returns:
            Response: A Flask JSON response with the active planes' IDs and status 200,
                      or an error message and status 500 if an exception is raised.
        """
        try:
            values = self.launcher.db.get_active_planes_info()
            if not values:
                return jsonify({'message': 'System has not detected any active planes so far.'}), 200

            string_values = [str(value[0]) for value in values]
            result = ', '.join(string_values)
            return jsonify({'message': f'ID of active planes: {result}.'}), 200
        except Exception as e:
            return jsonify({'Error during get info about active planes': str({e})}), 500

    def get_specific_plane_info(self, plane_id):
        """
        Retrieve detailed information about a specific plane.

        This method fetches information about a plane based on its ID using `Launcher` class.
        If provided plane ID is invalid (less than 1), it returns an error message. If
        plane is not found, it returns a 404 status with a message. If the plane is found, it
        returns information about whether it is connected, collided, or landed. If an error
        occurs, it returns an error message and a 500 status code.

        Parameters:
            plane_id (int): The ID of the plane to retrieve information for.

        Returns:
            Response: A Flask JSON response with the plane's details and status 200, or
                      an error message and status 404 or 500 if an exception is raised.
        """
        try:
            if plane_id < 1:
                return jsonify({'Error': 'Invalid plane ID. It must be a positive integer.'})

            plane_info = self.launcher.db.get_specific_plane_info(plane_id)
            if not plane_info:
                return jsonify({'message': f'Plane with ID: {plane_id} not found.'}), 404

            connected, collided, landed = plane_info[0], plane_info[1], plane_info[2]
            return jsonify({'message': f'Plane ID: {plane_id}, '
                                       f'Connected: {connected}, '
                                       f'Collided: {collided}, '
                                       f'Landed: {landed}'}), 200
        except Exception as e:
            return jsonify({f'Error during get info about plane with ID {plane_id}': str({e})}, 500)

    def run_flask(self):
        """
        Run the Flask server.

        This method starts the Flask server on port 5001. The `use_reloader=False` flag is
        set to disable auto-reloading, which can cause issues when working with threads.
        """
        self.app.run(debug=False, port=5001, use_reloader=False)

    def run(self):
        """
        Start the Flask server and the airport system visualization.

        This method runs the Flask server in a separate thread and starts airport
        system's visualization using `Launcher` class.
        """
        flask_thread = Thread(target=self.run_flask)
        flask_thread.start()
        self.launcher.start_visualisation()


if __name__ == '__main__':
    api = AirportSystemAPI()
    api.run()
