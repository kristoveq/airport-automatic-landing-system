from flask import Flask, request
from app import Launcher
from jsonrpcserver import method, dispatch, Success, Error


class AirportSystemRPC:
    """
    Initializes an instance of the AirportSystemRPC class.

    The constructor creates a Flask application instance, initializes the `Launcher` object,
    and sets up RPC methods and the route for the JSON-RPC server.

    Raises:
        Exception: Handles any errors that occur during the initialization of components.
    """
    def __init__(self):
        self.app = Flask(__name__)
        self.launcher = Launcher()
        self.setup_methods()
        self.setup_route()

    def setup_methods(self):
        """
        Configures RPC methods for the airport system.

        This method creates several JSON-RPC decorated functions used to
        control system operations, such as starting, pausing, and shutting down
        the system, as well as retrieving uptime, collision, and landing information.

        Raises:
        Exception: Returns error details if issues occur during method setup.
        """
        @method
        def start_system():
            """
            Starts the airport system.

            This method initiates all background tasks of the system using the `Launcher` object.
            On success, it returns a success message; on failure, it returns an error message.

            Returns:
                str: Success message if the system starts successfully or an error message otherwise.
            """
            try:
                self.launcher.start_background_tasks()
                return Success('Airport system started successfully.')
            except Exception as e:
                return Error(f'Error during starting airport system: {str(e)}')

        @method
        def pause_system():
            """
            Pauses the airport system.

            This method pauses all active background tasks, sets the system's paused flag,
            and logs the pause action. It ensures that no new operations proceed until the system is resumed.

            Returns:
                str: Success message indicating the system has been paused or an error message if the operation fails.
            """
            try:
                self.launcher.pause_system()
                return Success('Airport system paused.')
            except Exception as e:
                return Error(f'Error during pausing system: {str(e)}')

        @method
        def resume_system():
            """
            Resumes the airport system.

            This method clears the system's paused flag, allowing all background tasks to continue,
            and logs the resume action. It ensures that the system operations are back to normal.

            Returns:
                str: Success message indicating the system has been resumed or an error message if the operation fails.
            """
            try:
                self.launcher.resume_system()
                return Success('Airport system paused')
            except Exception as e:
                return Error(f'Error during pausing system: {str(e)}')

        @method
        def shutdown_system():
            """
            Shuts down the airport system.

            This method stops all background tasks, sets system's stop event, and performs cleanup operations.
            It ensures that resources are released properly and logs the shutdown process.

            Returns:
                str: Success message indicating system has been shut down or an error message if operation fails.
            """
            try:
                self.launcher.shutdown_system()
                return Success('Airport system shutdown initiated.')
            except Exception as e:
                return Error(f'Error during shutting down the system: {str(e)}')

        @method
        def uptime_info():
            """
            Retrieves the system's uptime.

            This method calculates time duration for which system has been running
            and returns formatted uptime in seconds.

            Returns:
                str: The system's uptime in seconds or an error message if operation fails.
            """
            try:
                result = self.launcher.server.uptime()
                return Success(f'Server uptime: {result} seconds.')
            except Exception as e:
                return Error(f'Error during get server uptime: {str(e)}')

        @method
        def active_amount():
            """
            Retrieves current number of active planes in airport's airspace.

            This method queries airport system to determine how many planes are
            currently active and returns this count.

            Returns:
                int: Number of active planes in airspace or an error message if operation fails.
            """
            try:
                result = len(self.launcher.server.airport.air_traffic)
                return Success(f'Amount of active planes: {result}.')
            except Exception as e:
                return Error(f'Error during getting amount of active planes: {str(e)}')

        @method
        def collisions_info():
            """
            Retrieves information about collisions that have occurred.

            This method fetches collision data from database and returns
            details, such as time and IDs of planes involved.

            Returns:
                list: A list of dictionaries containing collision information or an error message if retrieval fails.
            """
            try:
                values = self.launcher.db.get_status_info('collision')
                if not values:
                    return Success('System has not detected any collisions so far.')
                string_values = [str(value[0]) for value in values]
                result = ', '.join(string_values)
                return Success(f'ID of collided planes: {result}.')
            except Exception as e:
                return Error(f'Error during get info about collisions: {str(e)}')

        @method
        def landings_info():
            """
            Retrieves information about all planes that have landed.

            This method queries the database for the IDs of planes that have successfully landed.
            If no landings are detected, it returns a message indicating this. Otherwise, it
            formats the IDs into a string and returns them.

            Returns:
                Success: A message containing the IDs of landed planes or a message indicating no landings have occurred.
                Error: An error message if the operation fails.
            """
            try:
                values = self.launcher.db.get_status_info('landed')
                if not values:
                    return Success('System has not detected any landings so far.')

                string_values = [str(value[0]) for value in values]
                result = ', '.join(string_values)
                return Success(f'ID of landed planes: {result}.')
            except Exception as e:
                return Error(f'Error during get info about landings: str({e}')

        @method
        def active_info():
            """
            Retrieves information about all currently active planes.

            This method queries the database for the IDs of planes that are currently active in the airspace.
            If no active planes are detected, it returns a message indicating this. Otherwise, it formats
            the IDs into a string and returns them.

            Returns:
                Success: A message containing the IDs of active planes or a message indicating no active planes
                are detected.
                Error: An error message if the operation fails.
            """
            try:
                values = self.launcher.db.get_active_planes_info()
                if not values:
                    return Success('System has not detected any active planes so far.')

                string_values = [str(value[0]) for value in values]
                result = ', '.join(string_values)
                return Success(f'ID of active planes: {result}.')
            except Exception as e:
                return Error(f'Error during get info about active planes: str({e})')

        @method
        def plane_info(plane_id):
            """
            Retrieves detailed information about a specific plane.

            This method fetches details about a plane with the given ID, such as its connection status,
            collision status, and landing status. It checks for valid plane IDs and returns an error
            if the ID is invalid or if the plane is not found.

            Parameters:
                plane_id (int): The ID of the plane to retrieve information about.

            Returns:
                Success: A message containing the plane's connection, collision, and landing status.
                Error: An error message if the plane ID is invalid, plane is not found, or if operation fails.
            """
            try:
                if plane_id < 1:
                    return Error('Invalid plane ID. It must be a positive integer.')

                plane_info = self.launcher.db.get_specific_plane_info(plane_id)
                if not plane_info:
                    return Error(f'Plane with ID: {plane_id} not found.', code=404)

                connected, collided, landed = plane_info[0], plane_info[1], plane_info[2]
                return Success(f'Plane ID: {plane_id}, '
                               f'Connected: {connected}, '
                               f'Collided: {collided}, '
                               f'Landed: {landed}')
            except Exception as e:
                return Error(f'Error during get info about plane with ID {plane_id} str({e})')

    def setup_route(self):
        """
        Sets up the Flask route for handling JSON-RPC requests.

        This method defines the `/rpc` route, which handles incoming POST requests,
        decodes the request data, dispatches it to JSON-RPC server for processing,
        and returns response.

        Returns:
            Response: The JSON-RPC response to be sent back to client.
        """
        @self.app.route('/rpc', methods=['POST'])
        def rpc():
            request_data = request.data.decode('utf-8')
            response = dispatch(request_data)
            return response

    def run(self):
        """
        Starts the Flask application.

        This method runs Flask app on default port 5001 with debugging disabled.
        It serves as entry point for JSON-RPC server.

        Returns:
            None
        """
        self.app.run(debug=False, port=5001)


if __name__ == '__main__':
    rpc = AirportSystemRPC()
    rpc.run()
