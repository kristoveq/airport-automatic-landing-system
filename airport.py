from logger import Logger
from datetime import datetime
log = Logger('Airport').get_logger()


class Airport:
    """
    A class representing an airport and its related operations.

    The Airport class handles the configuration of the airport's physical dimensions,
    landing approach paths, landing strips, waiting corridors, and air traffic information.
    """

    def __init__(self):
        """
            Initialize a new Airport instance.

            Attributes:
                start (datetime): The start time of the airport operations.
                axis_x (int): The x-axis dimension of the airport.
                axis_y (int): The y-axis dimension of the airport.
                axis_z (int): The z-axis dimension of the airport.
                capacity (int): The maximum capacity of the airport.
                landing_approach_path_a (dict): The configuration for landing approach path A.
                landing_strip_a (dict): The coordinates for landing strip A.
                landing_approach_path_b (dict): The configuration for landing approach path B.
                landing_strip_b (dict): The coordinates for landing strip B.
                waiting_corridors (list): The list of waiting corridors.
                air_traffic (dict): A dictionary to store current air traffic information.
            """
        self.start = datetime.now().replace(microsecond=0)
        self.axis_x = 10000
        self.axis_y = 10000
        self.axis_z = 5000
        self.capacity = 100
        self.landing_approach_path_a = {'marker': 'A', 'x': 3000, 'y': 500, 'z': 2000, 'busy': False}
        self.landing_strip_a = {'x': 3000, 'y': 5000, 'z': 0}
        self.landing_approach_path_b = {'marker': 'B', 'x': 7000, 'y': 500, 'z': 2000, 'busy': False}
        self.landing_strip_b = {'x': 7000, 'y': 5000, 'z': 0}
        self.waiting_corridors = self.create_waiting_corridors()
        self.air_traffic = {}

    @staticmethod
    def create_waiting_corridors():
        """
        Create and return a list of waiting corridors.

        This static method generates a list of waiting corridors with predefined z-coordinates
        and a busy flag set to False. It handles any exceptions that occur during the creation process.

        Returns:
            list: A list of dictionaries representing the waiting corridors.

        Raises:
            Exception: Logs any unexpected errors that occur during the creation of waiting corridors.
        """
        corridors = []
        try:
            for x in range(2030, 5000 + 1, 30):
                result = {'z': x, 'busy': False}
                corridors.append(result)
        except ValueError as e:
            log.error(f'Wrong value during create_waiting_corridors: {e}')
        except TypeError as e:
            log.error(f'Wrong type of data during create_waiting_corridors: {e}')
        return corridors

    def set_waiting_corridor_coordinates(self, object_id):
        """
        Set the coordinates for a waiting corridor and mark it as busy.

        This method finds the first available (not busy) waiting corridor and marks it as busy with the given object ID.
        It returns the coordinates for the waiting corridor. If no free corridors are available, it logs a message and returns False.

        Parameters:
            object_id (Any): The ID of the object that will occupy the waiting corridor.

        Returns:
            dict: A dictionary with the coordinates (x, y, z) of the waiting corridor if available.
            bool: False if no free waiting corridors are available or an error occurs.

        Raises:
            Exception: Logs any unexpected errors that occur during the setting of waiting corridor coordinates.
        """
        try:
            free_corridor = next((single for single in self.waiting_corridors if not single['busy']))
            free_corridor['busy'] = object_id
            x = 5000
            y = x
            z = free_corridor['z']
            return {'x': x, 'y': y, 'z': z}
        except StopIteration as e:
            log.error(f'No free waiting corridors found: {e}')
            return False
        except KeyError as e:
            log.error(f'There\'s no required keys in dict data: {e}')
            return False

    def check_landing_corridors(self):
        """
            Checks the landing corridors for available paths.

            This method inspects the landing approach paths 'A' and 'B' to determine if any of them are not busy.
            If an available path is found, it returns a list containing the marker, the coordinates of the path,
            and the corresponding landing strip. If no path is available, it returns False. In case of an error,
            it logs the error and returns False.

            Returns:
                list: A list containing the marker ('A' or 'B'), the coordinates (dict with keys 'x', 'y', 'z'),
                and the corresponding landing strip if an available path is found.
                bool: False if no path is available or if an error occurs.

            Raises:
                Exception: Logs the error if any exception occurs during the check.
            """
        try:
            paths = {'A': self.landing_approach_path_a, 'B': self.landing_approach_path_b}
            landing_strips = {'A': self.landing_strip_a, 'B': self.landing_strip_b}
            for marker, path in paths.items():
                if not path['busy']:
                    coordinates = {'x': path['x'], 'y': path['y'], 'z': path['z']}
                    return [marker, coordinates, landing_strips[marker]]
            return False
        except KeyError as e:
            log.error(f'There is no keys in "path" or "landing_strips: {e}')
            return False
        except AttributeError as e:
            log.error(f'No success in accessing to attributes: {e}')
            return False

    def set_landing_corridor(self, marker, identification):
        """
            Sets a landing corridor as busy with the given identification.

            This method iterates through the landing approach paths and sets the 'busy' status of the path identified
            by the given marker. If the marker is not found, it raises a ValueError. In case of an error,
            it logs the error.

            Args:
                marker (str): The marker of the landing corridor to set (e.g., 'A' or 'B').
                identification (str): The identification to set for the 'busy' status of the path.

            Raises:
                ValueError: If no path with the specified marker is found.
                Exception: Logs the error if any exception occurs during the operation.

            """
        paths = [self.landing_approach_path_a, self.landing_approach_path_b]
        try:
            for path in paths:
                if path['marker'] == marker:
                    path['busy'] = identification
                    break
            else:
                raise ValueError(f'Did\'t find path with marker {marker}.')
        except ValueError as e:
            log.error(f'Path with specific marker not found: {e}')
            return False

    def release_landing_corridor(self, identification):
        """
            Releases a landing corridor that matches the given identification.

            This method iterates through the landing approach paths and sets the 'busy'
            status to False for the path identified by the given identification. If the
            identification is not found, it raises a ValueError. In case of an error,
            it logs the error.

            Args:
                identification (str): The identification of the landing corridor to release.

            Raises:
                ValueError: If no path with the specified identification is found.
                Exception: Logs the error if any exception occurs during the operation.
            """
        paths = [self.landing_approach_path_a, self.landing_approach_path_b]
        try:
            for path in paths:
                if path['busy'] == identification:
                    path['busy'] = False
                    break
            else:
                log.warning(f'Trying to release a corridor that is not occupied by plane {identification}.')
        except KeyError as e:
            log.error(f'ID: {identification}: doesn\'t exist in "paths" {e}')
            return False

    def check_distance(self, plane_id, plane_position):
        """
            Checks the distance between the given plane and other planes in the air traffic.

            This method iterates through the air traffic and compares the positions of
            other planes with the given plane to determine proximity warnings or potential
            collisions. It returns a proximity warning if another plane is within a certain
            range and a collision warning if another plane is too close.

            Args:
                plane_id (str): The ID of the plane to check.
                plane_position (dict): The position of the plane with keys 'x', 'y', and 'z'.

            Returns:
                str: 'Proximity warning' if another plane is within the specified range.
                str: 'Collision' if plane is collided with another one.
                bool: False if no proximity warning or collision is detected.

            Raises:
                KeyError: If the position dictionary of the plane is missing required keys.
                Exception: Logs the error if any exception occurs during the operation.
            """
        try:
            for other_id, other_position in self.air_traffic.items():
                if plane_id != other_id:
                    if (11 <= abs(plane_position['x'] - other_position['x']) <= 50 and
                            11 <= abs(plane_position['y'] - other_position['y']) <= 50 and
                            11 <= abs(plane_position['z'] - other_position['z']) <= 20):
                        return 'Proximity warning'
                    elif (abs(plane_position['x'] - other_position['x']) <= 10 and
                          abs(plane_position['y'] - other_position['y']) <= 10 and
                          abs(plane_position['z'] - other_position['z']) <= 10):
                        return 'Collision'
            return False
        except KeyError:
            log.error(f'Missing required keys in plane {plane_id} position dict: {plane_position}')
            return False
        except TypeError as e:
            log.error(f'Unexpected data type appeared during distance calculation: {e}')
            return False
