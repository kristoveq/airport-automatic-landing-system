import threading
from data_base.database import DatabaseHandler
from threading import Thread, Event
import time
import random
from server import Server
from client import Client
from visualisation import AirportVisualiser


class Launcher:
    def __init__(self):
        self.stop_event = threading.Event()
        self.server = Server()
        self.clients = []
        self.client_threads = []
        self.paused = False
        self.paused_event = Event()
        self.db = DatabaseHandler('APP')
        self.db.connect = self.db.open_connection()
        self.visualiser = AirportVisualiser(self.server.airport,
                                            self.server.airport.air_traffic,
                                            self.server.pos_lock,
                                            lambda: self.paused)

    def start_server(self):
        print('Try to connect server')
        self.server.connect()
        print('Server connected')
        while not self.stop_event.is_set():
            if self.paused:
                self.paused_event.wait()
                continue
            print('Server started')
            self.server.start()
        print('Server stopped.')

    def start_clients(self):
        while not self.stop_event.is_set():
            if self.paused:
                self.paused_event.wait()
                continue
            interval = random.uniform(0.5, 3)
            if self.paused_event.wait(timeout=interval):
                continue
            if self.stop_event.is_set():
                break
            client = Client(paused_event=self.paused_event)
            client_thread = Thread(target=client.run, daemon=True)
            client_thread.start()
            self.clients.append(client)
            self.client_threads.append(client_thread)

    def pause_system(self):
        print('Pausing the system...')
        self.paused = True
        self.paused_event.set()
        self.server.paused = True

    def paused_flag(self):
        return self.paused

    def shutdown_system(self):
        print('Shutting down system...')
        self.stop_event.set()
        self.paused = True
        self.paused_event.set()
        for client in self.clients:
            if client.plane and client.plane.target_pos:
                client.shutdown_client()
        for client_thread in self.client_threads:
            client_thread.join()
        self.server.shutdown_server()
        if self.server_thread.is_alive():
            self.server_thread.join()
        self.visualiser.running = False
        print('System shutdown complete.')

    def resume_system(self):
        print('Resuming system...')
        self.paused = False
        self.paused_event.clear()
        self.server.paused = False

    def start_visualisation(self):
        self.visualiser.run()

    def start_background_tasks(self):
        self.server_thread = Thread(target=self.start_server, daemon=True)
        self.clients_thread = Thread(target=self.start_clients, daemon=True)

        self.server_thread.start()
        self.clients_thread.start()


if __name__ == '__main__':
    launcher = Launcher()
    launcher.start_background_tasks()
    launcher.start_visualisation()

