import socket
import technical_assumptions
from data_tools import JsonHandler
from plane import Plane
import threading
from logger import Logger
log = Logger('Client').get_logger()


class Client:
    """
       A client for connecting to a server and handling communication.

       The Client class handles the setup of a socket connection to a server,
       sending and receiving data, and processing the received information.
       """
    def __init__(self, host='127.0.0.1', port=60000, paused_event=None):
        """
               Initialize a new Client instance.

               Parameters:
               host (str): The IP address of the host. Default is '127.0.0.1'.
               port (int): The port number to connect to. Default is 60000.

               Attributes:
               host (str): The IP address of the host.
               port (int): The port number to connect to.
               buffer (int): The buffer size for receiving data.
               client_socket (socket.socket): The client socket for network communication.
               data_format (JsonHandler): The handler for formatting data.
               id (int): The client ID.
               plane (Plane): The plane object associated with the client.
               running (bool): The flag indicating if the client is running.
               current_info (Any): The current information received from the server.
               """
        self.host = host
        self.port = port
        self.buffer = 1024
        self.client_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.client_socket.settimeout(technical_assumptions.refreshing_condition)
        self.data_format = JsonHandler()
        self.id = None
        self.paused_event = paused_event
        self.plane = Plane(paused_event=self.paused_event)
        self.stop_event = threading.Event()
        self.current_info = None

    def run(self):
        """
        Run the main loop of the client.

        This method establishes a connection to the server and enters the main loop,
        during which it continuously sends the current position, listens for incoming data,
        and processes confirmations. If an exception occurs, it logs the error and
        performs cleanup.

        Raises:
        Exception: Logs and handles any exception that occurs during the main loop.
        """
        self.connect()
        while not self.stop_event.is_set():
            try:
                self.send_current_position()
                self.listening()
                self.get_corridor_confirmation()
                self.status_confirmation()
            except Exception as e:
                log.error(f'Closing client socket. Error during main loop: {e}')
                self.clean_up()

    def shutdown_client(self):
        print(f'Shutting down client no. {self.id}...')
        if self.plane:
            self.plane.shutdown_plane()
        self.stop_event.set()

        if self.client_socket:
            try:
                self.client_socket.shutdown(socket.SHUT_RDWR)
                self.client_socket.close()
            except Exception as e:
                log.error(f'Error during closing client socket for client {self.id}: {e}')
            finally:
                self.client_socket = None
        else:
            log.warning(f'Socket for client {self.id} is already closed!')
        log.info(f'Client {self.id} shutdown complete.')

    def connect(self):
        """
               Establish a connection to the server.

               This method attempts to connect the client socket to the specified host and port.
               If the connection fails, it logs the error and sets the running flag to False.

               Raises:
               Exception: Logs any exception that occurs during the connection attempt.
               """
        try:
            self.client_socket.connect((self.host, self.port))
        except Exception as e:
            log.error(f'Client connection failed: {e}')
            self.stop_event.set()

    def take_data(self):
        """
        Receive data from the server.

        This method attempts to receive data from the server using the client socket.
        It decodes the received data and processes it using the data format handler.
        If the data is received, it is decoded and returned.

        Returns:
            message (Any): The decoded message if data is received, otherwise None.

        Raises:
            Exception: Logs any exception that occurs during data reception.
            socket.timeout: Returns None if the socket times out.
        """
        data = None
        if not self.stop_event.is_set():
            try:
                data = self.client_socket.recv(self.buffer).decode('utf-8')
                if not data:
                    self.stop_event.set()
                    log.info(f'Server closed the connection for client {self.id}.')
                    return None
                else:
                    message = self.data_format.decode(data)
                    return message
            except socket.timeout:
                return None
            except Exception as e:
                log.error(f'{data}Client {self.id} cannot take data from server: {e}')
                self.stop_event.set()
                self.clean_up()
                return None

    def send_data(self, data):
        """
        Send data to the server.

        This method encodes the provided data using the data format handler,
        appends a newline character, and sends it to the server through the client socket.

        Parameters:
            data (Any): The data to be sent to the server.

        Raises:
            Exception: Logs any exception that occurs during data transmission.
        """
        if self.stop_event.is_set():
            return
        if self.client_socket:
            try:
                result = self.data_format.encode(data)
                full_result = result + '\n'
                self.client_socket.send(full_result.encode('utf-8'))
            except Exception as e:
                log.error(f'Client {self.id} cannot send data to server: {e}')
                self.stop_event.set()
                self.clean_up()
        else:
            log.warning(f'Cannot send data, client socket is None for client {self.id}')

    def listening(self):
        """
        Listen for incoming data from the server.

        This method continuously listens for incoming data by calling the `take_data` method.
        If a message is received, it processes the message. If an exception occurs,
        it logs the error and stops the client's main loop.

        Raises:
            Exception: Logs any exception that occurs during the listening process.
        """
        try:
            if self.stop_event.is_set():
                return
            message = self.take_data()
            if message:
                self.process_message(message)
        except Exception as e:
            log.error(f'Error in listening method (Client {self.id}): {e}')
            self.stop_event.set()

    def process_message(self, message):
        """
        Process a received message from the server.

        This method processes a message by checking for the presence of the 'msg' key.
        If the key is missing, it logs an error. It then retrieves the appropriate action
        based on the message content and executes it. If the action is found, it updates
        the current information and sends a data confirmation.

        Parameters:
            message (dict): The message received from the server.

        Raises:
            Exception: Logs any exception that occurs during message processing.
        """
        try:
            if 'msg' not in message:
                log.error(f'Message format error: "msg" key missing in {message}')
                return
            instructions = self.instructions()
            action = instructions.get(message['msg'])
            if action:
                self.current_info = {key: value for key, value in message.items() if key != 'msg'}
                self.send_data_confirmation(message['msg'])
                action()
            else:
                log.info(f'Unknown command: {message["msg"]}')
        except Exception as e:
            log.error(f'Error processing message {message}: {e}')

    def send_data_confirmation(self, msg_type):
        """
        Send a confirmation message to the server.

        This method sends a confirmation message indicating that a specific type of message
        has been processed. The confirmation message contains the 'msg' key set to 'Confirmed'
        and the 'type' key set to the type of the original message.

        Parameters:
            msg_type (str): The type of the message to confirm.

        Raises:
            Exception: Logs any exception that occurs during sending the confirmation message.
        """
        try:
            confirmation_data = {'msg': 'Confirmed', 'type': msg_type}
            self.send_data(confirmation_data)
        except Exception as e:
            log.error(f'Client {self.id} cannot send data confirmation: {e}')
            self.stop_event.set()

    def send_current_position(self):
        """
        Send the current position of the plane to the server.

        This method checks if the plane is running. If it is, it sends the current position
        of the plane by calling the `send_data` method with a message containing the
        'msg' key set to 'Position' and the 'pos' key set to the plane's current position.

        Raises:
            Exception: Logs any exception that occurs during sending the current position data.
        """
        if not self.plane.stop_event.is_set():
            try:
                self.send_data({'msg': 'Position', 'pos': self.plane.position})
            except Exception as e:
                log.error(f'Client {self.id} cannot send current position data: {e}')
                self.stop_event.set()

    def instructions(self):
        """
        Provide a dictionary of instructions mapping message types to corresponding actions.

        This method returns a dictionary where the keys are message types received from the server,
        and the values are the methods to be executed for each message type.

        Returns:
            dict: A dictionary mapping message types to methods.
        """
        result = {
            'Occupied airspace': self.access_denied,
            'Welcome': self.access_granted,
            'Go to position': self.data_transmission_to_await,
            'Free path': self.landing_path_data_transmission,
            'Free landing strip': self.landing_strip_data_transmission,
            'Proximity warning': self.turn_on_rapid_response_function,
            'Collision': self.collision_detected,
            'Server shutting down': self.shutdown_client
        }
        return result

    def collision_detected(self):
        """
        Handle collision detection event.

        This method logs a warning message indicating a collision has been detected,
        sets the plane's collided flag, and performs cleanup operations.

        Raises:
            Exception: Logs any exception that occurs during the collision handling process.
        """
        log.warning(f'ID {self.id} - COLLISION')
        self.plane.collided.set()
        self.clean_up()

    def turn_on_rapid_response_function(self):
        """
        Handle proximity warning event.

        This method logs a warning message indicating a proximity warning has been detected
        and sets the plane's at_risk flag.

        Raises:
            Exception: Logs any exception that occurs during the proximity warning handling process.
        """
        log.warning(f'ID {self.id} - PROXIMITY WARNING')
        self.plane.at_risk.set()

    def access_denied(self):
        """
        Handle access denied event.

        This method stops the client's main loop by setting the running flag to False
        and performs cleanup operations.
        """
        self.stop_event.set()
        self.clean_up()

    def access_granted(self):
        """
        Handle access granted event.

        This method sets the client's and plane's ID from the current information received,
        and logs an access granted message. If an AttributeError or KeyError occurs,
        it handles the exception appropriately.

        Raises:
            AttributeError: If there is an attribute error during the process.
            KeyError: If there is a key error during the process.
        """
        try:
            self.id = self.current_info['id']
            self.plane.id = self.current_info['id']
            log.info(f'ACCESS GRANTED - {self.id}')
        except (AttributeError, KeyError) as e:
            self.handle_key_and_attribute_exception(e)

    def data_transmission_to_await(self):
        """
        Handle data transmission to set the plane's target position and start it.

        This method sets the plane's target position from the current information received,
        and starts the plane's operations. If an AttributeError or KeyError occurs,
        it handles the exception appropriately.

        Raises:
            AttributeError: If there is an attribute error during the process.
            KeyError: If there is a key error during the process.
        """
        try:
            self.plane.target_pos = self.current_info
            self.plane.start()
        except (AttributeError, KeyError) as e:
            self.handle_key_and_attribute_exception(e)

    def landing_path_data_transmission(self):
        """
        Handle landing path data transmission to set the plane's target position and permit landing.

        This method sets the plane's landing permit and target position from the current information received.
        If an AttributeError or KeyError occurs, it handles the exception appropriately.

        Raises:
            AttributeError: If there is an attribute error during the process.
            KeyError: If there is a key error during the process.
        """
        try:
            self.plane.landing_permit.set()
            self.plane.target_pos = self.current_info
        except (AttributeError, KeyError) as e:
            self.handle_key_and_attribute_exception(e)

    def landing_strip_data_transmission(self):
        """
        Handle landing strip data transmission to set the plane's landing coordinates.

        This method sets the plane's landing coordinates from the current information received.
        If an AttributeError or KeyError occurs, it handles the exception appropriately.

        Raises:
            AttributeError: If there is an attribute error during the process.
            KeyError: If there is a key error during the process.
        """
        try:
            self.plane.landing_coordinates = self.current_info
        except (AttributeError, KeyError) as e:
            self.handle_key_and_attribute_exception(e)

    def get_corridor_confirmation(self):
        """
        Get confirmation that the plane is in the designated corridor and send it to the server.

        This method checks if the plane is in the designated corridor. If it is, it sends a confirmation
        message to the server and clears the in_corridor flag. If an exception occurs, it logs the error.

        Raises:
            Exception: Logs any exception that occurs during the corridor confirmation process.
        """
        try:
            if self.plane.in_corridor.is_set():
                data = {'msg': 'Confirmed', 'type': 'In corridor'}
                self.send_data(data)
                self.plane.in_corridor.clear()
        except Exception as e:
            log.error(f'Error during getting corridor confirmation: {e}')

    def status_confirmation(self):
        """
        Send status confirmation to the server based on the plane's current state.

        This method checks if the plane has landed or collided. Depending on the plane's state,
        it sends the appropriate confirmation message to the server and performs cleanup operations.
        If an exception occurs, it logs the error.

        Raises:
            Exception: Logs any exception that occurs during the status confirmation process.
        """
        try:
            if self.plane.landed.is_set():
                data = {'msg': 'Confirmed', 'type': 'Landed'}
            elif self.plane.collided.is_set():
                data = {'msg': 'Confirmed', 'type': 'Collision'}
            else:
                return
            self.send_data(data)
            self.clean_up()
        except Exception as e:
            log.error(f'Error during status confirmation: {e}')

    def clean_up(self):
        """
        Perform cleanup operations for the client.

        This method stops the client's main loop, closes the client socket, and logs any errors that occur
        during the cleanup process.

        Raises:
            Exception: Logs any exception that occurs during the cleanup process.
        """
        self.stop_event.set()
        if self.client_socket:
            try:
                self.client_socket.close()
            except Exception as e:
                log.error(f'Error during clean up: {e}')
                self.client_socket = None

    def handle_key_and_attribute_exception(self, e):
        """
        Handle AttributeError and KeyError exceptions.

        This method logs detailed error messages for AttributeError and KeyError exceptions,
        including the current information context.

        Parameters:
            e (Exception): The exception to handle.

        Raises:
            AttributeError: Logs the error if it is an AttributeError.
            KeyError: Logs the error if it is a KeyError.
        """
        if isinstance(e, AttributeError):
            log.error(f'Current info is: {self.current_info}: {e}')
        elif isinstance(e, KeyError):
            log.error(f'Missing key in current_info: {e}')


# if __name__ == '__main__':
#     while True:
#         interval = random.uniform(0.5, 3)
#         time.sleep(interval)
#         client = Client()
#         threading.Thread(target=client.run).start()
