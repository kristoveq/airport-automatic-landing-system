from logger import Logger
import threading
from threading import Lock
from data_base.database import DatabaseHandler
log = Logger('Connection Pool').get_logger()


class ConnectionPool:
    def __init__(self, min_connections, max_connections):
        self.min_connections = min_connections
        self.max_connections = max_connections
        self.lock = Lock()
        self.pool = []
        self.running = True

        with self.lock:
            for counter in range(self.min_connections):
                connection = self.create_connection()
                self.pool.append(connection)

    def create_connection(self):
        connection = DatabaseHandler(len(self.pool) + 1)
        try:
            connection.open_connection()
        except Exception as e:
            log.error(f'Failed to open connection: {e}')
            raise
        return connection

    def get_connection(self):
        with self.lock:
            connection = next((conn for conn in self.pool if not conn.active), None)
            if connection is not None:
                connection.active = True
                return connection
            if len(self.pool) < self.max_connections:
                try:
                    additional_conn = self.create_connection()
                    self.pool.append(additional_conn)
                    additional_conn.active = True
                    return additional_conn
                except Exception as e:
                    log.error(f'Failed to create additional conn: {e}')
            else:
                log.info(f'Reach limit ({self.max_connections}) of connections.')

    def release_connection(self, connection):
        with self.lock:
            if connection in self.pool:
                try:
                    connection.active = False
                except Exception as e:
                    log.error(f'Error during releasing db connection: {e}')

            else:
                log.info(f'Attempted to release connection not in pool: {connection.conn_id}')

    def clean_up_pool(self):
        with self.lock:
            for single_conn in list(self.pool):
                if not single_conn.active and len(self.pool) > self.min_connections:
                    try:
                        single_conn.close_connection()
                        self.pool.remove(single_conn)
                        log.info(f'CLEANED: CONNECTION ID: {single_conn.conn_id}')
                    except Exception as e:
                        log.error(f'Error during closing connection no. {single_conn.conn_id}: {e}')
            threading.Timer(60, self.clean_up_pool).start()


connection_pool = ConnectionPool(10, 100)
threading.Timer(60, connection_pool.clean_up_pool).start()
