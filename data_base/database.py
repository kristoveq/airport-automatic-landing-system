import configparser
import os.path
import psycopg2
from psycopg2 import Error
from threading import Lock
from logger import Logger
log = Logger('Database').get_logger()


class DatabaseConfiguration:
    def __init__(self, filename='airport_database.ini'):
        self.filename = filename
        self.section = 'AIRPORT'
        self.host = '127.0.0.1'
        self.port = '5000'
        self.user = 'postgres'
        self.password = 'junior'
        self.database = 'airport'

    def create_config(self):
        config = configparser.ConfigParser()
        config[self.section] = {
            'host': self.host,
            'port': self.port,
            'user': self.user,
            'password': self.password,
            'database': 'airport'
        }
        return config

    def save_config(self):
        if not os.path.exists(self.filename):
            with open(self.filename, 'w') as config_file:
                self.create_config().write(config_file)

    @staticmethod
    def load_config(filename, section):
        current_path = os.getcwd()
        full_path = os.path.join(current_path, filename)
        parser = configparser.ConfigParser()
        parser.read(full_path)

        database = {}
        if parser.has_section(section):
            params = parser.items(section)
            for param in params:
                database[param[0]] = param[1]
        else:
            raise Exception(f'Section {section} has not found in the file: {filename}.')
        return database


class DatabaseHandler:
    def __init__(self, conn_id):
        self.config = DatabaseConfiguration()
        self.config.save_config()
        self.params = self.config.load_config(self.config.filename, self.config.section)
        self.connect = None
        self.cursor = None
        self.active = False
        self.conn_id = conn_id
        self.lock = Lock()

    def open_connection(self):
        try:
            self.connect = psycopg2.connect(**self.params)
            self.connect.autocommit = False
            self.cursor = self.connect.cursor()
            self.active = False
        except (Exception, Error) as error:
            log.error(f'Connection to database failed: \n{error}')

    def close_connection(self):
        if self.connect is not None:
            try:
                self.cursor.close()
                self.connect.close()
            except (Exception, Error) as error:
                log.error(f'Can\'t close connection to database: \n{error}')

    def ensure_database_exists(self):
        try:
            temp_conn = psycopg2.connect(dbname='postgres', user=self.params['user'], password=self.params['password'],
                                         host=self.params['host'], port=self.params['port'])
            temp_conn.autocommit = True
            cursor = temp_conn.cursor()
            cursor.execute(f"SELECT 1 FROM pg_database WHERE datname = '{self.params['database']}'")
            if not cursor.fetchone():
                cursor.execute(f"CREATE DATABASE {self.params['database']}")
                log.info(f'Database {self.params["database"]} created')
            cursor.close()
            temp_conn.close()
        except (Exception, Error) as error:
            log.error(f'Error ensuring database exists: \n{error}')

    def create_airport_table(self):
        try:
            self.cursor.execute("""
                CREATE TABLE IF NOT EXISTS airport (
                    id INTEGER PRIMARY KEY,
                    connected BOOLEAN NOT NULL DEFAULT TRUE,
                    collision BOOLEAN NOT NULL DEFAULT FALSE,
                    landed BOOLEAN NOT NULL DEFAULT FALSE,
                    time_of_appearance TIMESTAMP DEFAULT CURRENT_TIMESTAMP
                );
            """)
            self.connect.commit()
            log.info("Table 'airport' created or already exists.")
        except psycopg2.Error as e:
            log.error(f'An error has occurred while creating the table: {e}')
            self.connect.rollback()

    def clear_table(self):
        try:
            query = 'TRUNCATE TABLE airport CASCADE'
            self.cursor.execute(query)
            self.connect.commit()
            log.info('Cleared table.')
        except (Exception, Error) as e:
            log.error(f'An error has occurred while deleting data from tables: {e}')
            self.connect.rollback()

    def set_starting_settings(self):
        self.ensure_database_exists()
        self.open_connection()
        if self.connect is not None:
            self.create_airport_table()
            self.clear_table()
            self.close_connection()

    def get_status_info(self, status):
        try:
            query = f'SELECT id FROM airport WHERE {status} = %s'
            values = (True, )
            with access.read_lock:
                self.cursor.execute(query, values)
                results = self.cursor.fetchall()
                return results
        except Exception as e:
            log.error(f'An error has occurred during getting collisions info: {e}')
            self.connect.rollback()

    def get_active_planes_info(self):
        try:
            query = 'SELECT id FROM airport WHERE connected = %s AND collision = %s AND landed = %s'
            values = (True, False, False)
            with access.read_lock:
                self.cursor.execute(query, values)
                results = self.cursor.fetchall()
                return results
        except Exception as e:
            log.error(f'An error has occurred during getting active planes info: {e}')
            self.connect.rollback()

    def get_specific_plane_info(self, id):
        try:
            query = 'SELECT connected, collision, landed FROM airport where id = %s'
            values = (id, )
            with access.read_lock:
                self.cursor.execute(query, values)
                result = self.cursor.fetchone()
                return result
        except Exception as e:
            log.error(f'An error has occurred during getting specific plane info: {e}')
            self.connect.rollback()


class PlaneHandler:
    def __init__(self, db_handler):
        self.db_handler = db_handler

    def insert_registration_data(self, id):
        try:
            query = 'INSERT INTO airport (id) VALUES (%s)'
            values = (id, )
            with access.insert_lock:
                self.db_handler.cursor.execute(query, values)
                self.db_handler.connect.commit()
        except (Exception, Error) as e:
            log.error(f'An error has occurred during inserting registration data: {e}')
            self.db_handler.connect.rollback()

    def update_specific_status(self, object_id, status, value):
        allowed_columns = ['collision', 'landed', 'connected']
        if status not in allowed_columns:
            raise ValueError('Invalid column name')
        try:
            query = f'UPDATE airport SET {status} = %s WHERE id = %s'
            values = (value, object_id)
            with access.update_lock:
                self.db_handler.cursor.execute(query, values)
                self.db_handler.connect.commit()
        except (Exception, Error) as e:
            log.error(f'An error has occurred during updating status data: {e}')
            self.db_handler.connect.rollback()


class DatabaseAccessor:
    def __init__(self):
        self.read_lock = Lock()
        self.insert_lock = Lock()
        self.update_lock = Lock()


access = DatabaseAccessor()
db = DatabaseHandler(0)
db.set_starting_settings()
