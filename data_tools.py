import json


class JsonHandler:
    """
        A handler class for encoding and decoding JSON data.

        This class provides methods to encode Python objects into JSON strings and
        decode JSON strings into Python objects. It handles exceptions by logging
        errors and returning None in case of failures.
    """

    def decode(self, json_data):
        """
            Decodes the given JSON string to a object.

            This method converts a JSON string into a dictionary or list using
            the `json.loads` function. If an error occurs during the decoding process,
            it prints an error message and returns None.

            Args:
                json_data (str): The JSON string to decode.

            Returns:
                dict or list: The decoded object if successful.
                None: If an error occurs during decoding.

            Raises:
                Exception: Prints the error if any exception occurs during the decoding.
            """
        try:
            data = json.loads(json_data)
            return data
        except Exception as e:
            print(f'Error during decoding from JSON: {e}')
            return None

    def encode(self, data):
        """
            Encodes the given data to a JSON string.

            This method converts a dictionary or list into a JSON string using the
            `json.dumps` function. If an error occurs during the encoding process, it
            prints an error message and returns None.

            Args:
                data (dict or list): The data to encode into JSON format.

            Returns:
                str: The JSON-encoded string if successful.
                None: If an error occurs during encoding.

            Raises:
                Exception: Prints the error if any exception occurs during the encoding.
            """
        try:
            json_data = json.dumps(data)
            return json_data
        except Exception as e:
            print(f'Error during encoding to JSON: {e}')
            return None
