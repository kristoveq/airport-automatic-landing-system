import logging


class Logger:
    """
    A custom logger class for setting up file and console logging.

    This class initializes a logger with specified name, log file, and log level.
    It sets up a file handler for logging warnings and above to a file, and a
    console handler for logging messages to the console. Both handlers use a
    common formatter for log messages.

    Attributes:
        logger (logging.Logger): The logger instance.
    """
    def __init__(self, name, log_file='app.log', level=logging.DEBUG):
        """
        Initializes the Logger instance with file and console handlers.

        Args:
            name (str): The name of the logger.
            log_file (str, optional): The log file name. Default is 'app.log'.
            level (int, optional): The log level for the console handler.
                                     Default is logging.DEBUG.
        """
        self.logger = logging.getLogger(name)
        self.logger.setLevel(level)
        self.logger.propagate = False

        file_handler = logging.FileHandler(log_file)
        file_handler.setLevel(logging.WARNING)

        console_handler = logging.StreamHandler()
        console_handler.setLevel(level)

        formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
        file_handler.setFormatter(formatter)
        console_handler.setFormatter(formatter)

        self.logger.addHandler(file_handler)
        self.logger.addHandler(console_handler)

    def get_logger(self):
        """
        Returns the logger instance.

        Returns:
            logging.Logger: The logger instance.
        """
        return self.logger
