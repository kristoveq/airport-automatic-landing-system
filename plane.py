import random
import numpy as np
import technical_assumptions
import threading
from data_base.connection_pool import connection_pool
from data_base.database import PlaneHandler
from logger import Logger
log = Logger('Plane').get_logger()


class Plane(threading.Thread):
    """
      A class representing a plane, inheriting from threading.Thread.

      This class handles various attributes and events related to the plane's
      state, position, and operations. It provides mechanisms for managing
      landing permits, collision risks, and other events relevant to a plane
      during its flight and landing process.
      """

    def __init__(self, paused_event=None):
        """
        Initialize Plane instance.

        Attributes:
        running (bool): Indicates if the plane is currently running.
        id (str or None): The identifier for the plane.
        speed (float): The speed of the plane, sourced from technical assumptions.
        fuel (float): The fuel level of the plane, sourced from technical assumptions.
        position (dict): The position of the plane.
        target_pos (dict or None): The target position of the plane.
        waiting_corridor (str or None): The waiting corridor for the plane.
        angle (int): The current angle of the flight.
        landing_coordinates (dict or None): The coordinates for landing.
        landing_permit (threading.Event): Event indicating if landing is permitted.
        in_corridor (threading.Event): Event indicating if the plane is in the corridor.
        landed (threading.Event): Event indicating if the plane has landed.
        at_risk (threading.Event): Event indicating if the plane is at risk of collision.
        collided (threading.Event): Event indicating if the plane has collided.
        db (DatabaseConnection or None): The database connection object for the plane.
        db_connection (DatabaseConnection or None): The actual database connection.
        """
        super().__init__()
        self.paused_event = paused_event
        self.stop_event = threading.Event()
        self.id = None
        self.speed = technical_assumptions.airplane_speed
        self.fuel = technical_assumptions.airplane_fuel
        self.position = self.set_starting_place()
        self.target_pos = None
        self.waiting_corridor = None
        self.angle = 0
        self.landing_coordinates = None
        self.landing_permit = threading.Event()
        self.in_corridor = threading.Event()
        self.landed = threading.Event()
        self.at_risk = threading.Event()
        self.collided = threading.Event()
        self.db = None
        self.db_connection = None

    def run(self):
        """
            Main thread execution method for the Plane class.

            This method represents the main lifecycle of the plane, including
            initialization, flight to the waiting zone, and the landing procedure.
        """
        self.initialisation_stage()
        if not self.stop_event.is_set():
            self.flight_to_waiting_zone()
            self.landing_procedure()

    def set_starting_place(self):
        """
        Sets the starting place for the plane.

        This method generates random coordinates for the plane's starting position.
        It randomly selects x and z coordinates within specific ranges and y
        coordinates from fixed values. If a ValueError occurs during the
        random number generation, it logs the error and returns None.

        Returns:
            dict: A dictionary containing the starting coordinates with keys 'x',
                  'y', and 'z'.
            None: If a ValueError occurs during coordinate generation.

        Raises:
            ValueError: Logs the error if any exception occurs during the random
                        coordinate generation.
        """
        try:
            x = random.randrange(0, 10000, self.speed)
            y = random.choice([0, 10000])
            z = random.randrange(2000, 5000, self.speed)
            return {'x': x, 'y': y, 'z': z}
        except ValueError as e:
            log.error(f'Value error during set_starting_place for PLANE {self.id}: {e}')
            return None

    def fly_to_the_assigned_coordinates(self):
        """
           Manages the plane's flight to the assigned target coordinates.

           This method continuously updates the plane's position towards the target
           coordinates while checking for safety events and handling fuel consumption.
           The loop runs until the plane reaches the target or stops running. If an
           error occurs, it logs the error and returns None.

           Returns:
               bool: True if the plane successfully flies to the assigned coordinates.
               None: If an error occurs during the flight.

           Raises:
               Exception: Logs the error if any exception occurs during the flight.
        """
        if not self.target_pos or not all(key in self.target_pos for key in ['x', 'y', 'z']):
            log.error(f"Invalid target_pos for PLANE {self.id}: {self.target_pos}")
            return None

        try:
            target_positions = {'x': self.target_pos['x'], 'y': self.target_pos['y'], 'z': self.target_pos['z']}
            while not self.stop_event.is_set() and not self.reached_target(target_positions):
                if self.paused_event and self.paused_event.is_set():
                    self.paused_event.wait()
                if self.check_and_handle_safety_events():
                    continue
                self.update_position(target_positions)
                self.fuel_consumption()
                if self.stop_event.wait(technical_assumptions.refreshing_condition):
                    break
            if self.paused_event and self.paused_event.is_set():
                self.paused_event.wait()
            return True
        except KeyError as e:
            log.error(f'KeyError during flight_to_assigned_coordinates in PLANE {self.id}: {e}')
            return None
        except ValueError as e:
            log.error(f'ValueError during flight_to_assigned_coordinates in PLANE {self.id}: {e}')
            return None

    def reached_target(self, target_positions):
        """
            Checks if the plane has reached the target coordinates.

            This method compares the plane's current position with the target coordinates
            and determines if the plane is within a range defined by its speed for each
            axis (x, y, z).

            Args:
                target_positions (dict): The target coordinates with keys 'x', 'y', and 'z'.

            Returns:
                bool: True if the plane's current position is within the range of the
                      target coordinates for each axis, otherwise False.
        """
        return all(abs(self.position[axis] - target_positions[axis]) <= self.speed for axis in target_positions)

    def check_and_handle_safety_events(self):
        """
            Checks and handles any safety events for the plane.

            This method checks if the plane is at risk or has collided. If the plane
            is at risk, it triggers the rapid response function. If the plane has
            collided, it triggers the collision handling function. The method returns
            True if any safety event is handled, otherwise it returns False.

            Returns:
                bool: True if a safety event is handled, otherwise False.
        """
        if self.at_risk.is_set():
            self.rapid_response_function()
            return True
        elif self.collided.is_set():
            self.collision()
            return True
        return False

    def update_position(self, target_positions):
        """
            Updates the plane's position towards the target coordinates.

            This method adjusts the plane's current position incrementally towards the
            target coordinates for each axis (x, y, z) based on the plane's speed.
            If the plane is within a single increment of the target position, it sets
            the position directly to the target.

            Args:
                target_positions (dict): The target coordinates with keys 'x', 'y', and 'z'.
        """
        try:
            for axis in target_positions:
                if abs(self.position[axis] - target_positions[axis]) < self.speed:
                    self.position[axis] = target_positions[axis]
                    continue
                if self.position[axis] > target_positions[axis]:
                    self.position[axis] -= self.speed
                else:
                    self.position[axis] += self.speed
        except KeyError as e:
            log.error(f'KeyError in update_position for PLANE {self.id}: Missing key {e}')
        except TypeError as e:
            log.error(f'TypeError in update_position for PLANE {self.id}: {e}')
        except Exception as e:
            log.error(f'Unexpected error in update_position for PLANE {self.id}: {e}')

    def circle_flight(self, r=200):
        """
            Executes a circular flight pattern until a landing permit is granted.

            This method makes the plane fly in a circular pattern around its current
            position with a given radius. The plane continues this circular movement
            until it receives a landing permit. During the flight, it checks for
            safety events and handles fuel consumption. The radius of the circular
            flight gradually increases if it is below 1500 units.

            Args:
                r (int, optional): The initial radius of the circular flight. Default is 200.

            Returns:
                bool: False if an exception occurs during the flight.

            Raises:
                Exception: Logs the error if any exception occurs during the circular flight.
        """
        try:
            center_x, center_y = self.position['x'], self.position['y']
            while not self.stop_event.is_set() and not self.landing_permit.is_set():
                if self.paused_event and self.paused_event.is_set():
                    self.paused_event.wait()
                if self.check_and_handle_safety_events():
                    continue
                self.circular_movement(center_x, center_y, r)
                if r < 1500:
                    r += 100
                self.fuel_consumption()
                if self.stop_event.wait(technical_assumptions.refreshing_condition):
                    break
        except KeyError as e:
            log.error(f'KeyError in circle_flight for PLANE {self.id}: Missing key {e}')
            return False
        except TypeError as e:
            log.error(f'TypeError in circle_flight for PLANE {self.id}: {e}')
            return False
        except Exception as e:
            log.error(f'Error during circle_flight in PLANE {self.id}: {e}')
            return False

    def circular_movement(self, center_x, center_y, r):
        """
            Moves the plane in a circular path around a specified center point.

            This method updates the plane's position to move in a circular trajectory
            with a given radius around the specified center coordinates (center_x, center_y).
            The plane's angle is incremented based on its speed and the radius of the
            circle.

            Args:
                center_x (int): The x-coordinate of the center of the circle.
                center_y (int): The y-coordinate of the center of the circle.
                r (int): The radius of the circular path.
        """
        distance = self.speed
        angle_change = distance / r
        self.angle += angle_change
        self.angle = self.angle % (2 * np.pi)
        x = round(center_x + np.cos(self.angle) * r)
        y = round(center_y + np.sin(self.angle) * r)
        self.position['x'], self.position['y'] = x, y

    def flight_to_waiting_zone(self):
        """
           Manages the plane's flight to the waiting zone.

           This method first directs the plane to fly to the assigned coordinates.
           If the plane successfully reaches the assigned coordinates, it then
           initiates a circular flight pattern to wait for further instructions.
        """
        if self.fly_to_the_assigned_coordinates():
            self.circle_flight()

    def into_air_corridor(self):
        """
        Directs the plane to fly into the assigned air corridor.

        This method directs plane to assigned coordinates of air corridor.
        Once plane reaches the assigned coordinates, it sets the `in_corridor`
        event to indicate that plane is now in corridor.
        """
        if self.paused_event and self.paused_event.is_set():
            self.paused_event.wait()
        if not self.stop_event.is_set():
            self.fly_to_the_assigned_coordinates()
            self.in_corridor.set()

    def landing_approach(self):
        """
        Manages plane's landing approach process.

        This method checks if landing permit is set and if landing coordinates
        are available. If both conditions are met, it sets target position to
        landing coordinates and directs plane to assigned coordinates.
        Upon successful landing, it updates plane's status in database,
        releases database connection, and logs landing event. If plane
        is running but necessary permit or coordinates is missing, it logs an error.

        Raises:
            Exception: Logs an error if the landing permit or coordinates are missing
                       and the plane is still running.
        """
        if self.paused_event and self.paused_event.is_set():
            self.paused_event.wait()
        if self.landing_permit.is_set() and self.landing_coordinates and not self.stop_event.is_set():
            self.target_pos = self.landing_coordinates
            if self.fly_to_the_assigned_coordinates():
                while self.paused_event and self.paused_event.is_set():
                    self.paused_event.wait()
                self.landed.set()
                self.db.update_specific_status(self.id, 'landed', 'True')
                self.stop_event.set()
                connection_pool.release_connection(self.db_connection)
                log.info(f'LANDED: {self.id}.\nFuel: {round(self.fuel)}')
        else:
            if not self.stop_event.is_set():
                log.error(f'ERROR: Missing landing permit or coordinates for plane {self.id}')

    def rapid_response_function(self):
        """
           Executes a rapid response maneuver to avoid immediate risks.

           This method performs an evade maneuver by randomly adjusting the plane's
           position on the x, y, and z axes. It then handles fuel consumption, clears
           the 'at risk' event, and waits for a specified condition refresh period.
           If a KeyError or any other exception occurs, it logs the error.

           Raises:
               KeyError: Logs the error if a KeyError occurs while updating the position.
               Exception: Logs the error if any other exception occurs during the maneuver.
        """
        try:
            x, y, z = random.choice([-20, 20]), random.choice([-20, 20]), random.choice([-20, 20])

            self.position['x'] += x
            self.position['y'] += y
            self.position['z'] += z
            self.fuel_consumption()
            self.at_risk.clear()
            if not self.stop_event.wait(technical_assumptions.refreshing_condition):
                return
            # Changed time.sleep with stop_event.wait() to allow immediate interruption
        except KeyError as e:
            log.error(f'KeyError during rapid_response_function in PLANE {self.id}: {e}')
        except Exception as e:
            log.error(f'Error during rapid_response_function in PLANE {self.id}: {e}')

    def collision(self):
        """
            Handles the plane's collision event.

            This method updates the plane's status to indicate a collision, sets the
            'collided' event, updates the collision and connection status in the
            database, releases the database connection, and logs a warning with
            the plane's ID and remaining fuel.
        """
        self.stop_event.set()
        # Change self.running with stop_event
        self.collided.set()
        if self.db:
            # ensure that plane have access to db
            self.db.update_specific_status(self.id, 'collision', True)
            self.db.update_specific_status(self.id, 'connected', False)
            # Updated correct status to db
            connection_pool.release_connection(self.db_connection)
            log.warning(f'COLLISION - PLANE {self.id} - Fuel: {self.fuel}')
        else:
            log.error(f'No database connection available to update collision status for PLANE {self.id}')
            # add log if plane haven't got access to db

    def fuel_consumption(self):
        """
        Handles the fuel consumption of the plane.

        This method decreases the plane's fuel by a predefined amount. If the
        fuel level drops to zero or below, it triggers the collision handling
        method to indicate that the plane has crashed due to fuel depletion.
        """
        self.fuel -= technical_assumptions.fuel_consumption
        if self.fuel <= 0:
            self.collision()

    def initialisation_stage(self):
        """
        Initializes the plane's operational state and database connection.

        This method sets the plane's running state to True, establishes a database
        connection, initializes the database handler, logs the plane's entry into
        the airspace, and inserts the plane's registration data into the database.
        """
        self.stop_event.clear()
        # Clear stop event to allow thread to run
        self.db_connection = connection_pool.get_connection()
        self.db = PlaneHandler(self.db_connection)
        log.info(f'Plane {self.id} in airspace of airport.')
        self.db.insert_registration_data(self.id)

    def landing_procedure(self):
        """
        Executes the plane's landing procedure.

        This method directs the plane into the air corridor, initiates the landing
        approach, and updates the plane's connection status in the database
        after the landing process is completed.
        """
        if not self.stop_event.is_set():
            # Change self.running to stop_event for better control
            self.into_air_corridor()
            self.landing_approach()
            self.db.update_specific_status(self.id, 'connected', False)
            # updated db to reflect current condition of plane connection

    def shutdown_plane(self):
        print(f'Shutdown plane {self.id}...')
        self.stop_event.set()
        # Set stop_event instead of self.running = False
        self.landing_permit.set()
        self.at_risk.set()
        self.collided.set()
        self.landed.set()
        self.in_corridor.set()
        if self.paused_event:
            self.paused_event.set()

        if self.is_alive():
            self.join(timeout=5)
            # set timeout
        if self.db_connection:
            connection_pool.release_connection(self.db_connection)
        log.info(f'Plane {self.id} shutdown complete.')
