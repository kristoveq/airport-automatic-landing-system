import socket
import time
from data_tools import JsonHandler
import threading
from airport import Airport
from logger import Logger
import logging
log = Logger('Server').get_logger()


class Server:
    """
    A server for managing connections and communications with clients.

    The Server class handles the setup of a socket server, managing client connections,
    and processing data received from clients.
    """
    def __init__(self):
        """
           Initialize a new Server instance.

           Attributes:
               self.host (str): The IP address of the host.
               self.port (int): The port number to bind the server socket to.
               self.buffer (int): The buffer size for receiving data.
               self.server_socket (socket.socket): The server socket for network communication.
               self.data_format (JsonHandler): The handler for formatting data.
               self.capacity (int): The maximum capacity of clients the server can handle.
               self.daily_arrivals (int): The number of client arrivals for the day.
               self.clients_list (list): The list of connected clients.
               self.airport (Airport): The airport object associated with the server.
               self.lock (threading.Lock): The lock for synchronizing access to shared resources.
               self.pos_lock (threading.Lock): The lock for synchronizing access to position data.
               self.running (bool): The flag indicating if the server is running.
           """
        super().__init__()
        self.host = '127.0.0.1'
        self.port = 60000
        self.buffer = 4096
        self.socket = None
        self.server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.socket_closed = False
        self.data_format = JsonHandler()
        self.capacity = 100
        self.daily_arrivals = 0
        self.clients_list = []
        self.airport = Airport()
        self.lock = threading.Lock()
        self.pos_lock = threading.Lock()
        self.stop_event = threading.Event()
        self.paused = False
        self.start_time = time.time()
        self.server_socket_closed = False

    def uptime(self):
        return round(time.time() - self.start_time, 1)

    def connect(self):
        """
        Set up the server socket and start listening for incoming connections.

        This method binds the server socket to the specified host and port, sets the maximum number of
        connections it can handle, and starts listening for incoming client connections. It logs
        informational messages and any errors that occur during the process.

        Raises:
            socket.error: Logs any socket-related errors that occur during setup.
            Exception: Logs any unexpected errors that occur during setup.
        """
        try:
            self.server_socket.bind((self.host, self.port))
            self.server_socket.listen(self.capacity)
            self.socket = self.server_socket
            log.info('Server connected')
        except socket.error as e:
            log.error(f'Socket error was occurred: {e}')
        except Exception as e:
            log.error(f"An unexpected error occurred: {e}")

    def close_socket(self):
        """
        Close the socket safely, ensuring no double closure and logging any errors.
        """
        if self.socket:
            try:
                if not self.socket_closed:
                    self.socket.shutdown(socket.SHUT_RDWR)
                    self.socket.close()
                    self.socket_closed = True
                    log.info('Socket closed successfully.')
                else:
                    log.warning('Socket already closed. Skipping shutdown.')
            except Exception as e:
                log.error(f'Error during closing socket: {e}')
            finally:
                self.socket = None
        else:
            log.warning('Attempted to close a None socket.')

    def new_connection(self):
        """
        Handle a new client connection.

        This method accepts a new client connection and creates a ClientHandler thread to manage it.
        If the airport's capacity is reached, it sends an "Occupied airspace" message and closes the connection.
        Otherwise, it adds the new client to the list and starts the thread.

        Raises:
            socket.error: Logs any socket-related errors that occur during the connection process.
            threading.ThreadError: Logs any threading errors that occur while starting the client handler.
            Exception: Logs any unexpected errors that occur during the connection process.
        """
        try:
            client_socket, client_address = self.server_socket.accept()
        except socket.error as e:
            log.error(f'Failed to accept new connection: {e}')
            return
        except Exception as e:
            log.error(f'Unexpected error occurred when accepting a new connection: {e}')
            return

        try:
            thread = ClientHandler(self.daily_arrivals + 1, client_socket, client_address, self)
            with self.pos_lock:
                if len(self.airport.air_traffic) >= self.capacity:
                    thread.send_data({'msg': 'Occupied airspace'})
                    thread.close_socket()
                else:
                    self.daily_arrivals += 1
                    self.clients_list.append(thread)
                    thread.start()
        except socket.error as e:
            log.error(f'Error communicating with client - {client_address}: {e}')
        except threading.ThreadError as e:
            log.error(f'Thread error with client - {client_address}: {e}')
        except Exception as e:
            log.error(f'Unexpected error occurred with client - {client_address}: {e}')

    def start(self):
        """
        Start the server's main loop to handle incoming connections.

        This method enters the main loop of the server, continuously accepting new client connections
        by calling the `new_connection` method. It handles socket errors and other unexpected errors by logging them
        and pausing briefly before retrying. It also handles a KeyboardInterrupt to shut down the server gracefully.

        Raises:
            socket.error: Logs any socket-related errors that occur during the main loop.
            Exception: Logs any unexpected errors that occur during the main loop.
            KeyboardInterrupt: Logs a message and stops the server when interrupted by the user.
        """
        while not self.stop_event.is_set():
            if self.paused:
                time.sleep(1)
                continue
            try:
                self.new_connection()
            except socket.error as e:
                log.error(f'Socket error occurred: {e}')
                time.sleep(1)
            except Exception as e:
                log.error(f'Unexpected error occurred: {e}')
                time.sleep(1)
            except KeyboardInterrupt:
                log.info(f'Server shutdown initiated by KeyboardInterrupt')
                self.stop_event.set()

    def shutdown_server(self):
        print('Shutting down server...')
        self.paused = True
        self.stop_event.set()

        for client_handler in self.clients_list:
            client_handler.send_shutdown_message()

        self.close_socket()

        for client_handler in self.clients_list:
            client_handler.shutdown_handler()

        for client_handler in self.clients_list:
            if client_handler.is_alive():
                client_handler.join()

        self.clients_list.clear()

        print('Server shutdown complete.')


class ClientHandler(threading.Thread, Server):
    """
    A thread for handling communication with a single client.

    The ClientHandler class manages the interaction between the server and a connected client,
    including receiving and sending data, and processing client requests.
    """
    def __init__(self, thread_id, client_socket, client_address, server):
        """
            Initialize a new ClientHandler instance.

            Parameters:
                thread_id (int): The unique identifier for this client handler thread.
                client_socket (socket.socket): The socket object for the client connection.
                client_address (tuple): The address of the connected client.

            Attributes:
                self.running (bool): A flag indicating if the client handler is running.
                self.server (Server): The server instance to which this client handler belongs.
                self.buffer (int): The buffer size for receiving data.
                self.recv_buffer (str): A buffer for temporarily storing received data.
                self.thread_id (int): The unique identifier for this client handler thread.
                self.client_socket (socket.socket): The socket object for the client connection.
                self.client_address (tuple): The address of the connected client.
                self.data_format (JsonHandler): The handler for formatting data.
                self.destination_data (Any): Data related to the client's destination.
                self.lock (threading.Lock): A lock for synchronizing access to shared resources.
                self.pos_lock (threading.Lock): A lock for synchronizing access to position data.
            """
        threading.Thread.__init__(self)
        super().__init__()
        self.stop_event = threading.Event()
        self.server = server
        self.buffer = 4096
        self.recv_buffer = ''
        self.thread_id = thread_id
        self.client_socket = client_socket
        self.client_address = client_address
        self.data_format = JsonHandler()
        self.destination_data = None
        self.lock = self.server.lock
        self.pos_lock = self.server.pos_lock
        self.socket = self.client_socket

    def run(self):
        """
        Run the main loop of the client handler thread.

        This method starts by sending identification data to the client. It then enters the main loop,
        continuously waiting for confirmation from the client. If an exception occurs during any
        of these operations, it logs the error.

        Raises:
            Exception: Logs any exception that occurs during sending identification data or waiting for confirmation.
        """
        try:
            self.send_identification_data()
        except Exception as e:
            log.error(f'Error in send_identification_data: {e}')

        while not self.stop_event.is_set():
            if self.server.paused:
                self.stop_event.wait(timeout=1)
                continue
            try:
                self.wait_for_confirmation()
            except Exception as e:
                log.error(f'Error in wait_for_confirmation: {e}')
        self.release_resources()
        self.closing_connection()

    def send_shutdown_message(self):
        if self.client_socket:
            try:
                data = {'msg': 'Server shutting down'}
                self.send_data(data)
            except Exception as e:
                log.error(f'Error sending shutdown message to client np. {self.thread_id}: {e}')

    def shutdown_handler(self):
        print(f'Shutting down client handler no. {self.thread_id}...')
        self.stop_event.set()
        self.close_socket()

    def wait_for_confirmation(self):
        """
        Wait for and process confirmation messages from the client.

        This method receives data from the client and processes it based on the message type.
        If the message is a confirmation, it handles the confirmation. If the message contains
        position data, it updates and checks the current position data. It logs warnings for
        unrecognized message types and errors for missing keys or other exceptions.

        Raises:
            KeyError: Logs any key errors that occur if expected keys are missing in the data.
            Exception: Logs any unexpected errors that occur during the waiting and processing of confirmation messages.
        """
        try:
            data = self.take_data()
            if data:
                if data['msg'] == 'Confirmed':
                    self.handle_confirmation(data['type'])
                elif data['msg'] == 'Position':
                    self.update_and_check_current_position_data(data['pos'])
                else:
                    log.warning(f'Client-thread no {self.thread_id} received unrecognized message type: {data["msg"]}')
            else:
                self.stop_event.set()
                log.info(f'No data received. Client-thread no {self.thread_id} may have closed the connection.')
        except KeyError as e:
            log.error(f'Missing expected key in data for client-thread no {self.thread_id}: {e}')
        except Exception as e:
            log.error(f'Unexpected error in wait_for_confirmation in client-thread no {self.thread_id}: {e}')

    def handle_confirmation(self, confirmation_type):
        """
        Handle different types of confirmation messages from the client.

        This method processes confirmation messages based on their type. It takes appropriate
        actions such as sending waiting positions, determining landing paths, sending landing strip data,
        releasing landing corridors, closing connections, and releasing resources.

        Parameters:
            confirmation_type (str): The type of confirmation received from the client.

        Raises:
            Exception: Logs any unexpected errors that occur during the handling of confirmation messages.
        """
        try:
            if confirmation_type == 'Welcome':
                self.send_waiting_position()
            elif confirmation_type == 'Go to position':
                self.determine_landing_path()
            elif confirmation_type == 'Free path':
                self.send_landing_strip_data()
            elif confirmation_type == 'In corridor':
                self.server.airport.release_landing_corridor(self.thread_id)
            elif confirmation_type == 'Landed' or confirmation_type == 'Collision':
                log.info(f'ID: {self.thread_id} - {confirmation_type}')
                self.closing_connection()
                self.release_resources()
            elif confirmation_type == 'Occupied airspace':
                self.closing_connection()
        except Exception as e:
            log.error(f'Error during handle_confirmation by client-thread no. {self.thread_id}: {e}')

    def send_identification_data(self):
        """
        Send identification data to the client.

        This method sends a welcome message with the thread's ID to the client. If the thread ID is None,
        it logs an error.

        Raises:
            Exception: Logs any exception that occurs during sending identification data.
        """
        if self.thread_id is None:
            log.error('Thread ID is empty or None.')
        data = {'msg': 'Welcome', 'id': self.thread_id}
        self.send_data(data)

    def update_and_check_current_position_data(self, position):
        """
        Update the current position of the client and check for potential conflicts.

        This method updates the client's current position in the server's air traffic data
        and checks the distance to other aircraft using the airport's check_distance method.
        If a status message is returned, it sends this data back to the client and logs a debug message.

        Parameters:
            position (Any): The current position data of the client.

        Raises:
            Exception: Logs any unexpected errors that occur during the update and checking of position data.
        """
        try:
            with self.pos_lock:
                self.server.airport.air_traffic[self.thread_id] = position
                current_position_status = self.server.airport.check_distance(self.thread_id, position)
                if current_position_status:
                    data = dict(msg=current_position_status)
                    self.send_data(data)
        except Exception as e:
            log.error(f'Error during update_and_check_position_data by client-thread no {self.thread_id}: {e}')

    def send_waiting_position(self):
        """
        Send the waiting position coordinates to the client.

        This method sets the waiting corridor coordinates for the client using the airport's method
        and sends these coordinates to the client. If the coordinates are empty, it logs an error.

        Raises:
            Exception: Logs any unexpected errors that occur during the setting or sending of coordinates.
        """
        with self.lock:
            coordinates = self.server.airport.set_waiting_corridor_coordinates(self.thread_id)
        if coordinates:
            coordinates['msg'] = 'Go to position'
            self.send_data(coordinates)
        else:
            log.error(f'Client-thread no {self.thread_id} received empty coordinates.')

    def send_landing_strip_data(self):
        """
        Send landing strip data to the client.

        This method sends the landing strip data to the client if it is available. If the
        destination data is empty, it logs an error.

        Raises:
            Exception: Logs any unexpected errors that occur during the sending of landing strip data.
        """
        if self.destination_data:
            self.send_data(self.destination_data)
        else:
            log.error(f'Client-thread no {self.thread_id} has empty destination_data.')

    def determine_landing_path(self):
        """
        Determine and process the landing path for the client.

        This method continuously checks for available landing corridors. If landing data is available
        and the current client handler is first in the server's client list, it processes the landing information.
        If position data is received, it updates and checks the current position data.

        Raises:
            IndexError: Logs an error if an index error occurs during the process.
            Exception: Logs any unexpected errors that occur during the determination of the landing path.
        """
        while not self.stop_event.is_set():
            landing_data = self.server.airport.check_landing_corridors()
            try:
                if landing_data and self.server.clients_list[0] == self:
                    self.process_landing_information(landing_data)
                    break
                else:
                    data = self.take_data()
                    if data:
                        if data['msg'] == 'Position':
                            self.update_and_check_current_position_data(data['pos'])
                        else:
                            log.warning(f'Client-thread no {self.thread_id} received unrecognized message type in determine_landing_path: {data}')
                    else:
                        self.stop_event.set()
                        log.info(
                            f'No data received in determine_landing_path. Client-thread no {self.thread_id} may have closed the connection.')
                        break
            except IndexError as e:
                print(f'Error during determining landing place. {e}')
                continue

    def process_landing_information(self, landing_data):
        """
        Process the landing information and update the client's state accordingly.

        This method processes the landing data by setting the landing corridor, removing the client
        from the server's client list, and preparing landing path and landing strip data to be sent to the client.

        Parameters:
            landing_data (list): The landing data containing marker, path information, and landing strip information.

        Raises:
            Exception: Logs any unexpected errors that occur during the processing of landing information.
        """
        if landing_data:
            with self.lock:
                marker = landing_data[0]
                self.server.airport.set_landing_corridor(marker, self.thread_id)
                self.server.clients_list.remove(self)
            landing_data[1]['msg'] = 'Free path'
            landing_data[2]['msg'] = 'Free landing strip'
            self.destination_data = landing_data[2]
            self.send_data(landing_data[1])
        else:
            log.error(f'Empty landing data received by client-thread no {self.thread_id}.')

    def take_data(self):
        """
        Receive and decode data from the client.

        This method receives data from the client socket, decodes it, and processes complete messages.
        If the connection is reset or any other exception occurs, it logs the error.

        Returns:
            dict: The decoded message from the client if a complete message is received, otherwise None.

        Raises:
            ConnectionResetError: Logs an error if the connection with the client is reset.
            Exception: Logs any unexpected errors that occur during the receiving of data.
        """
        if not self.stop_event.is_set():
            try:
                data = self.client_socket.recv(self.buffer)
                if not data:
                    self.stop_event.set()
                    log.info(f'Client-thread no {self.thread_id} closed the connection.')
                    return None
                self.recv_buffer += data.decode('utf-8')
                if '\n' in self.recv_buffer:
                    message, _, self.recv_buffer = self.recv_buffer.partition('\n')
                    return self.data_format.decode(message)
            except ConnectionResetError as e:
                log.error(f'Connection with client-thread no {self.thread_id} has been reset: {e}')
            except Exception as e:
                log.error(f'Failed to receive data by client-thread no. {self.thread_id}: {e}')
        return None

    def send_data(self, data):
        """
        Send encoded data to the client.

        This method encodes the given data using the data format handler and sends it to the client
        through the client socket. If an exception occurs, it logs the error.

        Parameters:
            data (Any): The data to be encoded and sent to the client.

        Raises:
            Exception: Logs any unexpected errors that occur during the sending of data.
        """
        if self.client_socket:
            try:
                json_data = self.data_format.encode(data)
                self.client_socket.send(json_data.encode('utf-8'))
            except Exception as e:
                logging.error(f'ID {self.thread_id} - {data}Error sending data: {e}')
                self.stop_event.set()
                self.close_socket()
        else:
            logging.warning(f'Cannot send data, client socket is None for client-thread no {self.thread_id}')

    def release_resources(self):
        """
        Release resources associated with the client.

        This method releases any resources that were allocated for the client, such as
        waiting corridors and air traffic data. It checks and updates the waiting corridors
        to mark them as not busy and removes the client's data from the air traffic information.

        Raises:
            Exception: Logs any unexpected errors that occur during the resource release process.
        """
        with self.lock:
            resources = self.server.airport.waiting_corridors
            if resources:
                for factor in resources:
                    if factor['busy'] == self.thread_id:
                        factor['busy'] = False
                try:
                    self.server.airport.air_traffic.pop(self.thread_id, None)
                except Exception as e:
                    log.error(f'Error during removing plane from air traffic: {e}')
            else:
                log.error('Empty resources list.')

    def closing_connection(self):
        """
        Close the client connection.

        This method closes the client socket and stops the client handler by setting the running flag to False.
        If an exception occurs during the process, it logs the error. If the socket is already closed,
        it logs a warning.

        Raises:
            Exception: Logs any unexpected errors that occur during the closing of the connection.
        """
        if self.client_socket:
            try:
                self.stop_event.set()
                self.client_socket.close()
            except Exception as e:
                log.error(f'Error during closing connection: {e}')
        else:
            log.warning(f'Socket client-thread no. {self.thread_id} is already closed!')

#
# server = Server()
# visualisation = AirportVisualiser(server.airport, server.airport.air_traffic, server.pos_lock)
# if __name__ == '__main__':
#     server.connect()
#     threading.Thread(target=server.start).start()
#     visualisation.run()
