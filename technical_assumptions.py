# To make the application run more smoothly, the time was converted as follows: 1 minute = 1 second

refreshing_condition = 0.1  # value which corresponding airplane position refreshment

airplane_speed = 500 * refreshing_condition  # m per second * refreshing condition

airplane_fuel = 800  # units. The aircraft has fuel for a 3-hour flight.
fuel_consumption = 1.6 * refreshing_condition  # units per second * refreshing condition

