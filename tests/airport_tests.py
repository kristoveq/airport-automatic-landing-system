from airport import Airport
import unittest
from unittest.mock import patch, MagicMock


class TestAirport(unittest.TestCase):
    def setUp(self):
        self.airport = Airport()

    def tearDown(self):
        del self.airport

    def test_create_waiting_corridors(self):
        expected_result = [{'z': x, 'busy': False} for x in range(2030, 5000 + 1, 30)]
        result = self.airport.create_waiting_corridors()
        self.assertEqual(expected_result, result)

    @patch('airport.log')
    def test_create_waiting_corridors_exception(self, mock_log):
        with patch('airport.range', side_effect=TypeError('Wrong type of data.')):
            result = self.airport.create_waiting_corridors()
            self.assertEqual(result, [])
            mock_log.error.assert_called_once_with('Error during create_waiting_corridors: Wrong type of data.')

    def test_set_waiting_corridors_coordinates(self):
        result = self.airport.set_waiting_corridor_coordinates('test_id')
        self.assertIsInstance(result, dict)

    def test_set_waiting_corridors_coordinates_all_busy(self):
        for single_corr in self.airport.waiting_corridors:
            single_corr['busy'] = 'busy'
        result = self.airport.set_waiting_corridor_coordinates('test_id')
        self.assertFalse(result)

    @patch('airport.log')
    def test_set_waiting_corridors_coordinates_exception(self, mock_log):
        self.airport.waiting_corridors = None
        result = self.airport.set_waiting_corridor_coordinates('test_id')
        self.assertFalse(result)
        mock_log.error_assert_called_once_with('Error during set_wait_corridor_coordinates:'
                                               ' \'NoneType\' object is not iterable')

    def test_check_landing_corridors(self):
        result = self.airport.check_landing_corridors()
        self.assertTrue(result)
        self.assertIn(result[0], ['A', 'B'])
        self.assertIsInstance(result[1], dict)
        self.assertIsInstance(result[2], dict)

    def test_check_landing_corridors_busy(self):
        self.airport.landing_approach_path_a['busy'] = 'busy'
        self.airport.landing_approach_path_b['busy'] = 'busy'
        result = self.airport.check_landing_corridors()
        self.assertFalse(result)

    @patch('airport.log')
    def test_check_landing_corridors_exception(self, mock_log):
        self.airport.landing_approach_path_a = None
        self.airport.landing_approach_path_b = None
        result = self.airport.check_landing_corridors()
        self.assertFalse(result)
        mock_log.error.assert_called_once()

    def test_set_landing_corridor(self):
        self.airport.set_landing_corridor('A', 'test_id')
        self.assertEqual(self.airport.landing_approach_path_a['busy'], 'test_id')

    @patch('airport.log')
    def test_set_landing_corridor_value_error(self, mock_log):
        self.airport.set_landing_corridor('X', 'test_id')
        mock_log.error.assert_called_once()

    def test_release_landing_corridor(self):
        self.airport.set_landing_corridor('A', 'test_id')
        self.airport.release_landing_corridor('test_id')
        self.assertFalse(self.airport.landing_approach_path_a['busy'])

    @patch('airport.log')
    def test_release_landing_corridor_value_error(self, mock_log):
        self.airport.release_landing_corridor('test_id')
        mock_log.error.assert_called_once()

        # {1: {'x': 3000.0, 'y': 7950.0, 'z': 2400.0}
    def test_check_distance_proximity_warning(self):
        self.airport.air_traffic['1'] = {'x': 1000, 'y': 1000, 'z': 1000}
        result = self.airport.check_distance('test_id', {'x': 950, 'y': 950, 'z': 980})
        self.assertEqual('Proximity warning', result)

    def test_check_distance_collision(self):
        self.airport.air_traffic['1'] = {'x': 1000, 'y': 1000, 'z': 1000}
        result = self.airport.check_distance('test_id', {'x': 990, 'y': 990, 'z': 990})
        self.assertEqual('Collision', result)

    def test_check_distance_false(self):
        result = self.airport.check_distance('test_id', {'x': 990, 'y': 990, 'z': 990})
        self.assertFalse(result)

    @patch('airport.log')
    def test_check_distance_exception(self, mock_log):
        self.airport.air_traffic['1'] = {'x': 1000, 'y': 1000, 'z': 1000}
        wrong_key = 'A'
        self.airport.check_distance('test_id', {wrong_key: 990, 'y': 990, 'z': 990})
        mock_log.error.assert_called_once()


if __name__ == '__main__':
    unittest.main()
