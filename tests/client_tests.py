import unittest
from unittest.mock import Mock, patch
from client import Client


class TestClient(unittest.TestCase):
    def setUp(self):
        self.client = Client()
        self.client.client_socket = Mock()
        self.client.port = 55555
        self.client.host = 'localhost'
        self.client.running = True

    def tearDown(self):
        self.client.client_socket.close()
        print('Socket closed')
        del self.client
        print('deleted client')

    def test(self):
        self.assertIsInstance(self.client, Client)

    # @patch('client.log.error')
    # def test_connect(self, mock_log_error):
    #     self.client.client_socket.connect.return_value = None
    #     self.client.connect()
    #     self.client.client_socket.connect.assert_called_once_with(('localhost', 55555))
    #     self.assertTrue(self.client.running)
    #     mock_log_error.assert_not_called()


if __name__ == '__main__':
    unittest.main()