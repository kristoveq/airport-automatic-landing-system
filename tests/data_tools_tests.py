import unittest
from data_tools import JsonHandler
import json


class TestDataTools(unittest.TestCase):

    def setUp(self):
        self.handler = JsonHandler()

    def tearDown(self):
        del self.handler

    def test_decode(self):
        factor = 'test'
        data = json.dumps(factor)
        result = self.handler.decode(data)
        self.assertEqual(result, factor)

    def test_decode_exception(self):
        invalid_value = 12345
        result = self.handler.decode(invalid_value)
        self.assertIsNone(result)

    def test_encode(self):
        factor = 'test'
        result = self.handler.encode(factor)
        self.assertEqual(result, json.dumps(factor))

    def test_encode_exception(self):
        data = [1, 2, 3, 4, 5]
        factor = set(data)
        result = self.handler.encode(factor)
        self.assertIsNone(result)


if __name__ == '__main__':
    unittest.main()
