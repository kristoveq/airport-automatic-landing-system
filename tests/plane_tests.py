import unittest
from unittest.mock import patch, MagicMock

import technical_assumptions
from plane import Plane


class TestPlane(unittest.TestCase):
    def setUp(self):
        self.test_plane = Plane()
        self.id = 'test'
        self.test_plane.speed = 10
        self.test_plane.running = True
        technical_assumptions.refreshing_condition = 0

    def tearDown(self):
        self.test_plane.running = False
        del self.test_plane

    def test_set_starting_place(self):
        result = self.test_plane.set_starting_place()
        self.assertIn('x', result)
        self.assertIn('y', result)
        self.assertIn('z', result)
        self.assertGreaterEqual(result['x'], 0)
        self.assertLessEqual(result['x'], 10000)
        self.assertGreaterEqual(result['z'], 2000)
        self.assertLessEqual(result['z'], 5000)
        self.assertIn(result['y'], [0, 10000])

    @patch('random.randrange')
    def test_set_starting_place_value_error(self, mock_randrange):
        mock_randrange.side_effect = ValueError('mocked exception')
        result = self.test_plane.set_starting_place()
        self.assertIsNone(result)

    def test_reached_target_true(self):
        result = self.test_plane.reached_target(self.test_plane.position)
        self.assertTrue(result)

    def test_reached_target_false(self):
        target = {'x': 5000, 'y': 5000, 'z': 5000}
        result = self.test_plane.reached_target(target)
        self.assertFalse(result)

    def test_check_and_handling_safety_events_false(self):
        result = self.test_plane.check_and_handle_safety_events()
        self.assertFalse(result)

    def test_check_and_handling_safety_events_at_risk(self):
        self.test_plane.at_risk.set()
        result = self.test_plane.check_and_handle_safety_events()
        self.assertTrue(result)

    @patch('plane.Plane.collision') # mocking plane.collision
    def test_check_and_handling_safety_events_collided(self, mock_collision):
        self.test_plane.collided.set()
        result = self.test_plane.check_and_handle_safety_events()
        mock_collision.assert_called_once()
        self.assertTrue(result)

    def test_update_position(self):
        starting_pos = self.test_plane.position.copy()
        target = {'x': self.test_plane.position['x'] + 10,
                  'y': self.test_plane.position['y'] + 10,
                  'z': self.test_plane.position['z'] + 10}
        self.test_plane.update_position(target)
        ending_pos = self.test_plane.position
        self.assertNotEqual(starting_pos, ending_pos)

    def test_circular_movement(self):
        start = self.test_plane.position.copy()
        self.test_plane.circular_movement(start['x'], start['y'], r=200)
        end = self.test_plane.position
        self.assertNotEqual(start, end)

    def test_flight_to_the_assigned_coordinates(self):
        self.test_plane.position = {'x': 0, 'y': 0, 'z': 0}
        self.test_plane.target_pos = {'x': 30, 'y': 30, 'z': 30}
        result = self.test_plane.fly_to_the_assigned_coordinates()
        self.assertTrue(result)
        self.assertAlmostEqual(self.test_plane.position['x'], self.test_plane.target_pos['x'],
                               delta=self.test_plane.speed)
        self.assertAlmostEqual(self.test_plane.position['y'], self.test_plane.target_pos['y'],
                               delta=self.test_plane.speed)
        self.assertAlmostEqual(self.test_plane.position['z'], self.test_plane.target_pos['z'],
                               delta=self.test_plane.speed)

    @patch.object(Plane, 'fly_to_the_assigned_coordinates', return_value=True)
    @patch.object(Plane, 'circle_flight')
    def test_flight_to_waiting_zone(self, mock_fly_to_the_assigned_coordinates, mock_circle_flight):
        self.test_plane.flight_to_waiting_zone()
        mock_fly_to_the_assigned_coordinates.assert_called_once()
        mock_circle_flight.assert_called_once()

    @patch.object(Plane, 'fly_to_the_assigned_coordinates')
    def test_into_air_corridor(self, mock_fly_to_the_assigned_coordinates):
        self.test_plane.into_air_corridor()
        mock_fly_to_the_assigned_coordinates.assert_called_once()
        self.assertTrue(self.test_plane.in_corridor.is_set())

    def test_rapid_response_function(self):
        self.test_plane.at_risk.set()
        before = self.test_plane.position.copy()
        self.test_plane.rapid_response_function()
        after = self.test_plane.position

        self.assertFalse(self.test_plane.at_risk.is_set())
        self.assertNotEqual(before['x'], after['x'])
        self.assertNotEqual(before['y'], after['y'])
        self.assertNotEqual(before['z'], after['z'])
        self.assertIn(after['x'] - before['x'], [-20, 20])
        self.assertIn(after['y'] - before['y'], [-20, 20])
        self.assertIn(after['z'] - before['z'], [-20, 20])

    @patch('plane.log.error')
    def test_rapid_response_function_key_error(self, mock_log_error):
        self.test_plane.position = {}
        self.test_plane.rapid_response_function()
        mock_log_error.assert_called_once()

    @patch('plane.log.error')
    def test_rapid_response_function_general_error(self, mock_log_error):
        with patch.object(self.test_plane, 'fuel_consumption', side_effect=AttributeError):
            self.test_plane.rapid_response_function()
        mock_log_error.assert_called_once()

    @patch('plane.connection_pool.release_connection')
    @patch('plane.log.warning')
    def test_collision(self, mock_log_warning, mock_release_connection):
        self.test_plane.running = True
        self.test_plane.collided.clear()
        self.test_plane.db = MagicMock()
        self.test_plane.db_connection = MagicMock()

        self.test_plane.collision()

        self.assertFalse(self.test_plane.running)
        self.assertTrue(self.test_plane.collided.is_set())
        self.test_plane.db.update_specific_status.assert_any_call(self.test_plane.id, 'collision', True)
        self.test_plane.db.update_specific_status.assert_any_call(self.test_plane.id, 'connected', False)
        mock_release_connection.assert_called_once_with(self.test_plane.db_connection)
        mock_log_warning.assert_called_once_with(f'COLLISION - PLANE {self.test_plane.id} - Fuel: {self.test_plane.fuel}')

    def test_fuel_consumption(self):
        technical_assumptions.fuel_consumption = 100
        before = self.test_plane.fuel
        self.test_plane.fuel_consumption()
        self.assertLess(self.test_plane.fuel, before)

    @patch.object(Plane, 'collision')
    def test_fuel_consumption_collision(self, mock_plane_collision):
        technical_assumptions.fuel_consumption = 100
        self.test_plane.fuel = 50
        self.test_plane.fuel_consumption()
        mock_plane_collision.assert_called_once()

    @patch('plane.PlaneHandler')
    @patch('plane.connection_pool.get_connection')
    @patch('plane.log.info')
    def test_initialisation_stage(self, mock_log_info, mock_get_connection, mock_plane_handler):
        mock_connection = MagicMock()
        mock_get_connection.return_value = mock_connection
        mock_db = MagicMock()
        mock_plane_handler.return_value = mock_db

        self.test_plane.initialisation_stage()

        self.assertTrue(self.test_plane.running)
        mock_get_connection.assert_called_once()
        mock_plane_handler.assert_called_once_with(mock_connection)
        mock_db.insert_registration_data.assert_called_once_with(self.test_plane.id)
        mock_log_info.assert_called_once_with(f'Plane {self.test_plane.id} in airspace of airport.')

    @patch.object(Plane, 'into_air_corridor')
    @patch.object(Plane, 'landing_approach')
    def test_landing_procedure(self, mock_landing_approach, mock_into_air_corridor):
        self.test_plane.db = MagicMock()
        self.test_plane.db_connection = MagicMock()
        self.test_plane.running = True

        self.test_plane.landing_procedure()

        mock_into_air_corridor.assert_called_once()
        mock_landing_approach.assert_called_once()
        self.test_plane.db.update_specific_status.assert_called_once_with(self.test_plane.id, 'connected',
                                                                          self.test_plane.running)


if __name__ == '__main__':
    unittest.main()
