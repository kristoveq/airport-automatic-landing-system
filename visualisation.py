import matplotlib.pyplot as plt
from matplotlib.animation import FuncAnimation
import threading


class AirportVisualiser:
    """
    A class for visualizing the positions of planes in an airport's airspace.

    This class provides methods to visualize airport and positions of
    planes in 3D space using Matplotlib. It manages thread safety with locks
    to ensure synchronized updates.

    Attributes:
        airport_object (Airport): The airport object containing airport data.
        figure: The Matplotlib figure object for plotting.
        axes: The 3D subplot for plotting.
        x (int): The x-axis dimension of the airport.
        y (int): The y-axis dimension of the airport.
        z (int): The z-axis dimension of the airport.
        visualiser_lock (threading.Lock): A lock for synchronizing updates to the visualizer.
        planes (dict): A dictionary containing the positions of planes.
        safe_lock (threading.Lock): A lock for ensuring thread safety when accessing shared resources.
        scatter: The scatter plot object for plane positions.
    """
    def __init__(self, airport, positions, safe_lock, paused_flag):
        """
        Initializes the AirportVisualiser instance.

        Args:
            airport (Airport): The airport object containing airport data.
            positions (dict): A dictionary containing the positions of planes.
            safe_lock (threading.Lock): A lock for ensuring thread safety when accessing shared resources.
        """
        self.airport_object = airport
        self.figure = plt.figure()
        self.axes = self.figure.add_subplot(111, projection='3d')
        self.x, self.y, self.z = airport.axis_x, airport.axis_y, airport.axis_z
        self.visualiser_lock = threading.Lock()
        self.planes = positions
        self.safe_lock = safe_lock
        self.scatter = None
        self.paused_flag = paused_flag
        self.animation = None
        self.running = True

    def axis_setting(self):
        """
        Sets the axis limits and labels for the 3D plot.

        This method configures the x, y, and z axis limits based on the airport
        dimensions. It also sets the labels for each axis to 'LENGTH', 'WIDTH',
        and 'HEIGHT' respectively.
        """
        self.axes.set_xlim([0, self.x])
        self.axes.set_ylim([0, self.y])
        self.axes.set_zlim([0, self.z])

        self.axes.set_xlabel('LENGTH')
        self.axes.set_ylabel('WIDTH')
        self.axes.set_zlabel('HEIGHT')

    def update(self, frame):
        """
        Updates the 3D plot with the current positions of planes.

        This method clears the current plot, sets the axis limits and labels, and
        then updates the scatter plot with the positions of the planes. It ensures
        thread safety by using a lock when accessing the plane positions.

        Args:
            frame (int): The current frame number (used for animations).
        """
        if self.paused_flag():
            return
        if not self.running:
            plt.close(self.figure)
            return
        try:
            # if len(self.planes) > 0:
            self.axes.clear()
            self.axis_setting()

            with self.safe_lock:
                x_vals = [pos['x'] for pos in self.planes.values()]
                y_vals = [pos['y'] for pos in self.planes.values()]
                z_vals = [pos['z'] for pos in self.planes.values()]
            self.scatter = self.axes.scatter(x_vals, y_vals, z_vals, color='blue', marker='o', s=2)
        except Exception as e:
            print(f'Error during visualisation actualization: {e}')

    def run(self):
        """
        Runs the visualizer to animate the 3D plot of plane positions.

        This method sets the axis limits and labels, then creates an animation
        that periodically updates the plot with the current positions of the planes.
        The animation updates every 100 milliseconds.
        """
        self.axis_setting()
        animation = FuncAnimation(self.figure, self.update, interval=100, blit=False, cache_frame_data=False)
        plt.show()
